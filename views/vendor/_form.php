<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\User;
use app\models\Status;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Vendor */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vendor-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'vendorName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fax')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

  
		<?= $form->field($model, 'startTime')->widget(DatePicker::classname(), [
					//'model' => $model,
					//'name' => 'proposedEndDate', 
					'value' => date('d-M-Y'),
					'options' => ['placeholder' => 'Select issue date ...'],
					'pluginOptions' => [
						'format' => 'yyyy-mm-dd',
						'todayHighlight' => true
	]]); ?>

    
		<?= $form->field($model, 'endTime')->widget(DatePicker::classname(), [
					//'model' => $model,
					//'name' => 'proposedEndDate', 
					'value' => date('d-M-Y'),
					'options' => ['placeholder' => 'Select issue date ...'],
					'pluginOptions' => [
						'format' => 'yyyy-mm-dd',
						'todayHighlight' => true
	]]); ?>

    <?= $form->field($model, 'status')->
				dropDownList(Status::getStatuses()) ?> 

    <?= $form->field($model, 'comment')->textInput(['maxlength' => true]) ?>
	<?= $form->field($model, 'comment')->textarea (array('maxlength' => true, 'rows' => 6, 'cols' => 50)); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? yii::t('yii','Create') : yii::t('yii','Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
