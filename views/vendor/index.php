<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel app\models\VendorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = yii::t('yii','Vendors');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vendor-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(yii::t('yii','Create Vendor'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
	<?php
	$gridColumns = [
			 'id',
            'vendorName',
            'email:email',
            'fax',
            'phone',];
			echo ExportMenu::widget([
			'dataProvider' => $dataProvider,
			'columns' => $gridColumns]);?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'vendorName',
            'email:email',
            'fax',
            'phone',
            // 'address',
            // 'startTime',
            // 'endTime',
            // 'status',
            // 'comment',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
