<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\User;
use app\models\Status;
use app\models\UserType;
use app\models\Department;
use kartik\date\DatePicker;
use kartik\icons\Icon;

	

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">
	
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->textInput()  ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'authKey')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'firstName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'lastName')->textInput(['maxlength' => true]) ?>

    
	<?= $form->field($model, 'department')->dropDownList(Department::getDepartment()) ?>


    
   
	<?php if (\Yii::$app->user->can('updateuserType')) {?>
	<?= $form->field($model, 'userType')->radioList(UserType::getUserTypes()); ?>
	<?php } ?>
	
	<?= $form->field($model, 'email')->widget(\yii\widgets\MaskedInput::className(), [
    'name' => 'input-36', 'clientOptions' => [
        'alias' =>  'email'
    ],
	]) ?>
	
    
		<?= $form->field($model, 'endDate')->widget(DatePicker::classname(), [
					//'model' => $model,
					//'name' => 'proposedEndDate', 
					'value' => date('d-M-Y'),
					'options' => ['placeholder' => 'Select issue date ...'],
					'pluginOptions' => [
						'format' => 'yyyy-mm-dd',
						'todayHighlight' => true
	]]); ?>


    
	<?= $form->field($model, 'status')->
				dropDownList(Status::getStatuses()) ?> 

    <?= $form->field($model, 'comment')->textarea (array('maxlength' => true, 'rows' => 6, 'cols' => 50)); ?>

	<?php if (\Yii::$app->user->can('updateRole')) {?>
	<?= $form->field($model, 'role')->radioList(User::getRoles()) ?>
	<?php } ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ?  yii::t('yii','Create') : yii::t('yii','Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
