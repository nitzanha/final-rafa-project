<?php

namespace app\models;

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use Yii;
use kartik\export\ExportMenu;
use kartik\icons\Icon;
use kartik\dynagrid\DynaGrid;
use yii\grid\GridView;
use app\models\Status;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */


//$this->title = 'Users';
$this->title = yii::t('yii', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index" >

    <h1><?= Html::encode($this->title) ?></h1>
<?php // echo $this->render('_search', ['model' => $searchModel]);  ?>

    <p>
<?= Html::a(yii::t('yii', 'Create User'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

<?php
$columns = [
   // ['class' => 'kartik\grid\SerialColumn', 'order' => DynaGrid::ORDER_FIX_LEFT],
    'id',
    //'username',
   // 'user.firstName',
    //'lastName',
    [
        'attribute' => 'endDate',
        //'filterType'=>GridView::FILTER_DATE,
        'format' => 'raw',
        'width' => '170px',
        'filterWidgetOptions' => [
            'pluginOptions' => ['format' => 'yyyy-mm-dd']
        ],
    ],
    [
        'class' => 'kartik\grid\BooleanColumn',
        'attribute' => 'status',
        'vAlign' => 'middle',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'order' => DynaGrid::ORDER_FIX_RIGHT
    ],
    ['class' => 'kartik\grid\CheckboxColumn', 'order' => DynaGrid::ORDER_FIX_RIGHT],
];

yii::trace('message?', print_r($dataProvider, true));
echo DynaGrid::widget([
    'columns' => $columns,
    'storage' => DynaGrid::TYPE_COOKIE,
    'theme' => 'panel-danger',
    'gridOptions' => [
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'panel' => ['heading' => '<h3 class="panel-title">Library</h3>'],
    ],
    'options' => ['id' => 'dynagrid-1'] // a unique identifier is important
]);
?>


    <?php /*
      $gridColumns = [
      //'id',
      //'username',
      'password',
      'authKey',
      'firstName',];
      echo ExportMenu::widget([
      'dataProvider' => $dataProvider,
      'columns' => $gridColumns]); */
    ?>

    <?php /* echo GridView::widget([
      'dataProvider' => $dataProvider,
      'filterModel' => $searchModel,

      'columns' => [
      ['class' => 'yii\grid\SerialColumn'],

      'id',
      'username',

      'password',
      //'authKey',
      'firstName',
      // 'lastName',
      // 'department',
      // 'userType',
      // 'email:email',
      // 'endDate',
      //'status',
      [
      'attribute' => 'status',

      'label' => 'Status',
      'value' => function($model){
      return $model->statusItem->name;
      },
      'filter'=>Html::dropDownList('UserSearch[name]', $status, $statuses,
      ['class'=>'form-control']),
      ],
      // 'comment',

      ['class' => 'yii\grid\ActionColumn'],
      ],

      ]



      );

     */ ?> 
</div>
