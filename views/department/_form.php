<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use app\models\Status;

/* @var $this yii\web\View */
/* @var $model app\models\Department */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="department-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'depId')->textInput() ?>

    <?= $form->field($model, 'depName')->textInput(['maxlength' => true]) ?>

    <?=
    $form->field($model, 'endDate')->widget(DatePicker::classname(), [
        //'model' => $model,
        //'name' => 'proposedEndDate', 
        'value' => date('d-M-Y'),
        'options' => ['placeholder' => 'Select issue date ...'],
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
            'todayHighlight' => true
    ]]);
    ?>


    <?= $form->field($model, 'location')->textInput(['maxlength' => true]) ?>


    <?= $form->field($model, 'status')->
        dropDownList(Status::getStatuses())
    ?> 

        <?= $form->field($model, 'comment')->textarea(array('maxlength' => true, 'rows' => 6, 'cols' => 50)); ?>

    <div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? yii::t('yii', 'Create') : yii::t('yii', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>

</div>
