<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MealSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Meals';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="meal-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Meal', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'name',
            'details',
            // 'vendorId',
            [
                'attribute' => 'vendorId',
                'label' => 'Vendor',
                'value' => function($model) {
                    return $model->vendorItem->vendorName;
                },
            //'filter'=>Html::dropDownList('UserSearch[name]', $status, $statuses, ['class'=>'form-control']),
            ],
            // 'categoryId',
            [
                'attribute' => 'categoryId',
                'label' => 'Category',
                'value' => function($model) {
                    return $model->categoryItem->categoryName;
                },
            //'filter'=>Html::dropDownList('UserSearch[name]', $status, $statuses, ['class'=>'form-control']),
            ],
            'mealExtras',
            // 'picture',
            // 'status',
            'comment',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>
</div>
