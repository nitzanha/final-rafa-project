<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Category;
use app\models\Vendor;
use app\models\Status;
//use kartik\widgets\FileInput;
//use kartik\FileInput;
use \kartik\file\FileInput;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Meal */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="meal-form">

<?php
$form = ActiveForm::begin([
   'options' => ['enctype' => 'multipart/form-data'] // important
]);
?>

<?=
    FileInput::widget([
        'name' => 'mealAttachment',
        'options' => ['accept' => 'file'],
        'pluginOptions' => [
            'showUpload' => true,
            'showPreview' => false,
            'browseLabel' => 'בחר קובץ',
            'language' => 'he',
            'removeLabel' => '',
            'allowedFileExtensions' => ['xls'],
            'mainClass' => 'input-group-md'
        ]
    ]);
?>    
    <div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? yii::t('yii', 'Create') : yii::t('yii', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    <?= Html::a(yii::t('yii', 'Import'), ['/meal/importMeal'], ['class'=>'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>


</div>
