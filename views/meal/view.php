<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Meal */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Meals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="meal-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?=
        Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ])
        ?>
    </p>
    <!--https://code.tutsplus.com/tutorials/how-to-program-with-yii2-uploading-files--cms-23511-->
    <?=
    //file_put_contents("/var/www/html/testYair.log", "model: " . print_r($model, true) . "\n", FILE_APPEND);
//file_put_contents("/var/www/html/testYair.log", "image_web_filename: " . $model->image_web_filename . "\n", FILE_APPEND);
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'details',
            //'vendorId',
            [ // the vendorId name 
                'label' => $model->attributeLabels()['vendorId'],
                'value' => $model->vendorItem->vendorName,
            ],
            // 'categoryId',
            [ // the categoryId name 
                'label' => $model->attributeLabels()['categoryId'],
                'value' => $model->categoryItem->categoryName,
            ],
            'mealExtras',
            [
                'attribute' => 'Image',
                'format' => 'raw',
                'value' => function($model) {
                    //if ($model->image_web_filename != '')

                    if (!empty($model->image_web_filename))
                    {
                        $imageName = 'http://' . Yii::$app->request->serverName . Yii::$app->getRequest()->getBaseUrl() . '/uploads/images/' . $model->image_web_filename;
                        return '<img src="' . $imageName . '" width="50px" height="auto">';
                    }
                    else
                        return 'no image';
                },
                   
                    
            ],
            [ // the status name 
                'label' => $model->attributeLabels()['status'],
                'value' => $model->statusItem->name,
            ],
            'comment',
        ],
    ])
    ?>

</div>
