<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Category;
use app\models\Vendor;
use app\models\Status;
use \kartik\file\FileInput;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Meal */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="meal-form">

<?php
$form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data'] // important
    ]);
?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'details')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'vendorId')->
        dropDownList(vendor::getVendors())
    ?>

    <?= $form->field($model, 'categoryId')->
        dropDownList(category::getCategories())
    ?>

    <?= $form->field($model, 'mealExtras')->textarea(array('maxlength' => true, 'rows' => 6, 'cols' => 50)) ?>

    <?= $form->field($model, 'status')->
        dropDownList(status::getStatuses())
    ?>

    <?= $form->field($model, 'comment')->textarea(array('maxlength' => true, 'rows' => 6, 'cols' => 50)) ?>

    <?php
    if (!empty($model->image_web_filename)) {
        $imageName = 'http://' . Yii::$app->request->serverName . Yii::$app->getRequest()->getBaseUrl() . '/uploads/images/' . $model->image_web_filename;
        echo '<img src="' . $imageName . '" width="50px" height="auto">';
    }
    ?>
    <?=
    $form->field($model, 'picture')->widget(FileInput::classname(), [
        'options' => ['accept' => 'picture/*'],
        'pluginOptions' => ['allowedFileExtensions' => ['jpg', 'gif', 'png'], 'showUpload' => false,]
    ]);
    ?>    
    <div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? yii::t('yii', 'Create') : yii::t('yii', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>


</div>
