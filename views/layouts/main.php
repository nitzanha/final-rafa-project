<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
        <?php $this->beginBody() ?>

        <div class="wrap">
            <?php
            NavBar::begin([
                'brandLabel' => 'RafaFood',
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);
            if (Yii::$app->user->isGuest)
            {
                echo Nav::widget([
                    'options' => ['class' => 'navbar-nav navbar-right'],
                    'items' => [
                        ['label' => yii::t('yii', 'Home'), 'url' => ['/site/index']],
                        ['label' => yii::t('yii', 'About'), 'url' => ['/site/about']],
                        ['label' => yii::t('yii', 'Contact'), 'url' => ['/site/contact']],
                        //['label' => yii::t('yii','Order'), 'url' => ['/order/display_orders']],
                        Yii::$app->user->isGuest ? (
                            ['label' => yii::t('yii', 'Login'), 'url' => ['/site/login']]
                            ) : (
                            '<li>'
                            . Html::beginForm(['/site/logout'], 'post')
                            . Html::submitButton(
                                'Logout (' . Yii::$app->user->identity->username . ')', ['class' => 'btn btn-link logout']
                            )
                            . Html::endForm()
                            . '</li>'
                            )
                    ],
                ]);
            }
            
            if (\Yii::$app->user->can('admin')){
                echo Nav::widget([
                    'items' => [
                        ['label' => 'Home','url' => ['site/index'],],
                        [
                            'label' => yii::t('yii', 'Admin menu'),
                            'items' => [
                                ['label' => yii::t('yii', 'Users'), 'url' => ['/user']],
                                ['label' => yii::t('yii', 'Vendors'), 'url' => ['/vendor']],
                                ['label' => yii::t('yii', 'Orders'), 'url' => ['/order/display_orders']],
                                ['label' => yii::t('yii', 'Departments'), 'url' => ['/department']],
                                ['label' => yii::t('yii', 'Categories'), 'url' => ['/category']],
                                ['label' => yii::t('yii', 'Meals'), 'url' => ['/meal']],
                                ['label' => yii::t('yii', 'Calender'), 'url' => ['/order/order']],
                                '<li class="divider"></li>',
                                '<li class="dropdown-header">Dropdown Header</li>',
                                ['label' => 'Level 1 - Dropdown B', 'url' => '#'],
                            ],
                        ],
                        Yii::$app->user->isGuest ? (
                            ['label' => yii::t('yii', 'Login'), 'url' => ['/site/login']]
                        ) : (
                            '<li>'
                            . Html::beginForm(['/site/logout'], 'post')
                            . Html::submitButton(
                                'Logout (' . Yii::$app->user->identity->username . ')', ['class' => 'btn btn-link logout']
                            )
                            . Html::endForm()
                            . '</li>'
                            )
                    ],
                    'options' => ['class' => 'navbar-nav'],
                ]);
            }
            else 
            {
                if (!Yii::$app->user->isGuest)
                {
                    echo Nav::widget([
                        'options' => ['class' => 'navbar-nav navbar-right'],
                        'items' => [
                            ['label' => yii::t('yii', 'Home'), 'url' => ['/site/index']],
                            ['label' => yii::t('yii', 'About'), 'url' => ['/site/about']],
                            ['label' => yii::t('yii', 'Contact'), 'url' => ['/site/contact']],
                            ['label' => yii::t('yii','Order'), 'url' => ['/order/display_orders']],
                            Yii::$app->user->isGuest ? (
                                ['label' => yii::t('yii', 'Login'), 'url' => ['/site/login']]
                                ) : (
                                '<li>'
                                . Html::beginForm(['/site/logout'], 'post')
                                . Html::submitButton(
                                    'Logout (' . Yii::$app->user->identity->username . ')', ['class' => 'btn btn-link logout']
                                )
                                . Html::endForm()
                                . '</li>'
                                )
                        ],
                    ]);                    
                    /*
                    echo Nav::widget([
                        'items' => [
                            [
                                'label' => 'Home',
                                'url' => ['site/login'],
                            ],
                        ],
                        'options' => ['class' => 'navbar-nav'],
                    ]);
                     * 
                     */
                }
            }
            ?>
            <div class="navbar-text pull-right">
            <?=
            \lajax\languagepicker\widgets\LanguagePicker::widget([
                'skin' => \lajax\languagepicker\widgets\LanguagePicker::SKIN_DROPDOWN,
                'size' => \lajax\languagepicker\widgets\LanguagePicker::SIZE_LARGE
            ]);
            ?>
            </div>


                <?php NavBar::end();
                ?>

            <div class="container">
            <?=
            Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ])
            ?>
                <?= $content ?>
            </div>
        </div>

        <footer class="footer">
            <div class="container">
                <p class="pull-left">&copy; RafaFood <?= date('Y') ?></p>

                <p class="pull-right"><?= Yii::powered() ?></p>
            </div>
        </footer>

<?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
