<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use kartik\daterange\DateRangePicker;
use kartik\grid\SerialColumn;
use yii\helpers\Url; 

/* @var $this yii\web\View */
/* @var $searchModel app\models\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Night Shift Approval';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="order-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php //echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Order', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    
    <?php
    $columns = [
            //['class' => 'yii\grid\SerialColumn'],
            ['class' => 'kartik\grid\SerialColumn'], 
            'userId', 
            'shift', 
            'firstName', 
            'lastName', 
            'department', 
            'orderDate',
            [
                'label' => 'orderApproved',
                'format' => 'raw',
                'value' => function($model, $key, $index, $column) {
                    if ($model['orderApproved'] == 0)
                    {
                        return Html::a('ממתין לאישור', ['order/update_night_shift', 'id' => $model["id"], 'approvedStatus' => "1"], ['class' => 'btn btn-danger']);
                    }
                    else 
                    {
                        return Html::a('מאושר', ['order/update_night_shift', 'id' => $model["id"], 'approvedStatus' => "0"], ['class' => 'btn btn-success']);
                    }
                },
            ],                
            ['class' => 'kartik\grid\ActionColumn']
        ];
    
    echo ExportMenu::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => $columns,
        'fontAwesome' => true,
        'dropdownOptions' => [
            'label' => 'Export All',
            'class' => 'btn btn-default'
        ]
    ]) . "<hr>\n<br><br>";

                
                
    echo GridView::widget([
        'dataProvider' => $dataProvider,
//        /'filterModel' => $searchModel,
        'filterRowOptions'=>['class'=>'kartik-sheet-style'],
        'responsive'=>true,
        'resizableColumns'=>true,
        'floatHeader'=>true,
        'hover'=>true,
        'pjax'=>true,
        'pjaxSettings'=>[
            'neverTimeout'=>true,                
        ], 
        'columns' => $columns,
    ]);
    ?>
</div>
<script type="text/javascript">
function apply_filter() {
    $('.grid-view').yiiGridView('applyFilter');
}
</script>