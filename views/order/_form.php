<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\Shift;
use app\models\Categorytype;
use app\models\Category;
use kartik\depdrop\DepDrop;
use yii\bootstrap\ButtonGroup

/* @var $this yii\web\View */
/* @var $model app\models\Order */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-form">

    <?php
    $form = ActiveForm::begin([
            'id' => 'getOrder-form',
        ])
    ?>    
    
    <div class="form-group field-order-shift required">
        <label class="control-label">בחר משמרת</label>
        <input type="hidden" name="Order[shift]" id="orderShiftID" value="">
        <div id="order-shift" aria-required="true"></div> 
    </div>

    <div class="form-group required" id="categoryTypeID">
        <label class="control-label">בחר סוג</label>
        <input type="hidden" name="Order[categoryTypeId]" id="orderCategoryTypeID" value="">
        <div id="order-categorytypeid" aria-required="true"></div>
    </div>
    
    <div class="clearfix" id='mealTypeID'>
        <label class="control-label">Category ID</label>
        <div class="form-group field-order-categoryid required">
            <input type="hidden" name="Order[categoryId]" id="orderCategoryID" value="">
            <div id="order-categoryid" aria-required="true">
                <div class="help-block"></div>
            </div>	
        </div>
    </div>

    <div id='mealID'>
        <div class="form-group field-order-mealId required">
            <label class="control-label">meals</label>
            <input type="hidden" name="Order[mealId]" id="orderMealID" value="">
            <div id="order-meal" aria-required="true">
                <table id="mealTable" class="table table-striped table-bordered table-sm table-responsive">
                    <thead class="thead-dark">
                        <tr>
                            <th id="ID">ID</th>
                            <th>name</th>
                            <th>תוספות</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
                <div class="help-block"></div>
            </div>	
        </div>
    </div>

    <?= $form->field($model, 'comment')->textarea (array('id' => 'orderComment', 'maxlength' => true, 'rows' => 6, 'cols' => 50)) ?>

    <div class="form-group">
        <?= Html::submitButton('Create', ['id' => 'submitCreate', 'class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script>
    $(document).ready(function () {
        var shift = "";
        var categoryTypeID = "";
        var orderCategoryID = "";
        var mealType = "";
        var mealID = "";
        
        $(document).on("beforeSubmit", "#getOrder-form", function () {
            // send data to actionSave by ajax request.
            return false; // Cancel form submitting.
        });

        $('#categoryTypeID').hide();
        $('#mealTypeID').hide();
        $('#mealID').hide();  
        $('#submitCreate').attr('disabled','disabled');
        
        var url = window.location.href;
        var res = url.split("/");  //split by space
        var targetDates = getParameterByName('dates');
        var employeeID = getParameterByName('employeeID');
        if (employeeID == "undefined")
            employeeID = ""        
        
        res.pop();  //remove last element
        res.pop();
        url = res.join("/") + "/shift/get_relevant_shifts";
        
        jQuery.ajax({
            type: 'get',
            data: {targetDates: targetDates, employeeID: employeeID},
            url: url,
            success: function (response) {
                response = removeHeader(response);
                $('#order-shift').html(response); 
                
                $('button[id^="shiftID"]').on('click', function() {  
                    shift = $(this).attr('value'); // Get the value attribute
                    $('#orderShiftID').val(shift);
                });
            },
            error: function () {
                console.log('failure');
            }
        });   

        $('#order-shift').click(function () {
           //alert(shift);
            $('#categoryTypeID').hide();
            $('#mealTypeID').hide();
            $('#mealID').hide(); 
            var url = window.location.href;
            var res = url.split("/");  //split by space
            res.pop();  //remove last element
            res.pop();
            url = res.join("/") + "/categorytype/get_category";
            jQuery.ajax({
                type: 'get',
                url: url,
                data: {shift: shift},
                success: function (response) {
                    response = removeHeader(response);
                    //console.log(response);
                    $('#order-categorytypeid').html(response);
                    $('#categoryTypeID').show();
                
                    $('button[id^="categoryTypeID"]').on('click', function() {  
                        categoryTypeID = $(this).attr('value'); // Get the value attribute
                        $('#orderCategoryTypeID').val(categoryTypeID);
                    });
                    //$('#categoryTypeID').show();
                    //$('#mealTypeID').show();
                    //return false;
                },
                error: function () {
                    console.log('failure');
                }
            });
        });  

        $('#order-categorytypeid').click(function () {
            //alert("categoryTypeID: " +categoryTypeID);
            $('#mealTypeID').hide();
            $('#mealID').hide();    
            var url = window.location.href;
            var res = url.split("/");  //split by space
            res.pop();  //remove last element
            res.pop();
            url = res.join("/") + "/category/get_meals_by_parameters";

            jQuery.ajax({
                type: 'post',
                data: {shift: shift, categoryType: categoryTypeID},
                url: url,
                success: function (response) {
                    response = removeHeader(response);
                    $('#order-categoryid').html(response);
                    $('#mealTypeID').show();
                    
					//need to display all of the options - כריכים, סלטים, טוסטים ושונות
                    $('button[id^="orderCategoryID"]').on('click', function() {  
						orderCategoryID = $(this).attr('value');// Get the value attribute
                        $('#orderCategoryID').val(orderCategoryID);
                    });                    
                },
                error: function () {
                    console.log('failure');
                }
            });            

        });

        $(document).on("click",".mealCategoryID",function(){    
            if (document.getElementById("mealTable").rows.length > 1)
            {
                for (var i = document.getElementById("mealTable").rows.length; i > 1;i--)
                {
                    //console.log("i" + i);
                    document.getElementById("mealTable").deleteRow(i - 1);
                }
            }
        
            mealType = orderCategoryID;
           // $('#orderCategoryID').val(mealType);
            //alert("mealType: " + mealType);
            var url = window.location.href;
            var res = url.split("/");  //split by space
            res.pop();  //remove last element
            res.pop();
            url = res.join("/") + "/meal/get_meals_by_parameters";
            
            jQuery.ajax({
                type: 'post',
                cache: false,
                data: {shift: shift, categoryType: categoryTypeID, mealType: orderCategoryID},
                url: url,
                success: function (response) {
                    response = removeHeader(response);
                    $('#mealTable').append(response);
                    //$('#order-meal').html(response);
                    $('#mealID').show();
                    /*
                    $('button[id^="mealsID"]').on('click', function() {  
                        mealID = $(this).attr('value'); // Get the value attribute
                        $('#orderMealID').val(mealID);
                    });                     
                    */
                },
                error: function () {
                    console.log('failure');
                }
            });            
        });
        
        $("#mealTable").on('click','td',function(){
             $("#mealTable tr").css('background', '');
             // get the current row
             var currentRow=$(this).closest("tr"); 

             var col1=currentRow.find("td:eq(0)").text(); // get current row 1st TD value
             //var col2=currentRow.find("td:eq(1)").text(); // get current row 2nd TD
            // var col3=currentRow.find("td:eq(2)").text(); // get current row 3rd TD
             //var data=col1+"\n"+col2+"\n";//+col3;

             //alert(data);
             currentRow.css('background', 'red');
             mealID = col1;
             
             if (mealID != "" && orderCategoryID != "" && shift != "" && categoryTypeID != "")
                 $('#submitCreate').removeAttr('disabled');
        });       
        
        $('#submitCreate').click(function () {
            var url = window.location.href;            
            var dates = getParameterByName('dates');

            var employeeID = getParameterByName('employeeID');
            if (employeeID == "undefined")
                employeeID = ""
            var mealExtras = $('#mealExtras').val();
            
            /*
            console.log("mealExtras: " + mealExtras);
            alert("mealExtras: " + mealExtras);
            
            throw new Error("Sorry, but you are too young for this movie");
            */
            var res = url.split("/");  //split by space
            res.pop();  //remove last element
            res.pop();
            url = res.join("/") + "/order/create";

            jQuery.ajax({
                type: 'post',
                //cache: false,
                data: {mealID: mealID, orderCategoryID: orderCategoryID, shift: shift, categoryTypeId: categoryTypeID, dates: dates, comment: $('#orderComment').val(), 
                    employeeID: employeeID, mealExtras: mealExtras},
                url: url,
                success: function (response) {
                    console.log('success');
                    var url = window.location.href;            
                    var res = url.split("/");  //split by space
                    res.pop();  //remove last element
                    res.pop();
                    url = res.join("/") + "/order/display_orders";                    
                    window.open(url + '?employeeID=' + employeeID, "_self");
                },
                error: function () {
                    console.log('failure');
                }
            }); 
        });
    });

    function removeHeader(response) {
        var n = response.indexOf("!@#");
        var response = response.substr(n + 3);
        //console.log('response: ' + response);

        var n = response.indexOf("!@#");
        var response = response.substr(0, n);

        return response;
    }

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
</script>