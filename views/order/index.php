<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use kartik\daterange\DateRangePicker;
use kartik\grid\SerialColumn;

/* @var $this yii\web\View */
/* @var $searchModel app\models\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Orders';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="order-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php //echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Order', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    
    <?php
    $columns = [
            //['class' => 'yii\grid\SerialColumn'],
            ['class' => 'kartik\grid\SerialColumn'], 
            'id',
            [
                'attribute' => 'firstName',
                'label' => 'user',
                'value' => 'user.firstName'
            ],
            [
                'attribute' => 'name',
                'label' => 'Meal',
                'value' => 'meal.name'
            ],
            [
                'attribute' => 'categoryTypeName',
                'label' => 'categoryTypeName',
                'value' => 'categoryType.categoryTypeName'
            ],
            [
                'attribute' => 'categoryName',
                'label' => 'category Name',
                'value' => 'category.categoryName'
            ],
            
            [
                'attribute' => 'shiftName',
                'format' => 'ntext',
                'label' => 'shift Name',
                'value' => 'shift.shiftName'
            ],
              
            [
                'label' => 'Vendor',
                'format' => 'ntext',
                'attribute' => 'vendorName',
                'value' => function($model) {
                    foreach ($model->vendor as $vendor) {
                        $vendorNames[] = $vendor->vendorName;
                    }
                    return implode("\n", $vendorNames);
                },
            ],
            //'mealId',
            //'orderDate',
            'orderExtras',                   
            [
                'attribute' => 'orderDate',
                'headerOptions' => ['style' => 'width:20%'],
                //'format'=>'date',
                'filterType'=> \kartik\grid\GridView::FILTER_DATE_RANGE,
                'filterWidgetOptions' => [
                    'presetDropdown' => true,
                    'pluginOptions' => [
                        'format' => 'YYYY-MM-DD',
                        'separator' => ' TO ',
                        'opens'=>'right',
                        'locale'=>['format' => 'YYYY-MM-DD'],
                    ] ,
                    'pluginEvents' => [
                        "apply.daterangepicker" => "function() { apply_filter('orderDate') }",
                    ] 
                ],
            ],                    
            //'lastUpdate',
            // 'shift',
            // 'categoryTypeId',
            // 'categoryId',
            // 'status',
            // 'comment',
            //['class' => 'yii\grid\ActionColumn']
            ['class' => 'kartik\grid\ActionColumn']
        ];
    
    echo ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $columns,
        'fontAwesome' => true,
        'dropdownOptions' => [
            'label' => 'Export All',
            'class' => 'btn btn-default'
        ]
    ]) . "<hr>\n<br><br><br><br><br><br>";

                
                
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'filterRowOptions'=>['class'=>'kartik-sheet-style'],
        'responsive'=>true,
        'resizableColumns'=>true,
        'floatHeader'=>true,
        //'showPageSummary' => true,
        'hover'=>true,
        'pjax'=>true,
        'pjaxSettings'=>[
            'neverTimeout'=>true,                
        ], 
        'columns' => $columns,

        /*
        [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            [
                'attribute' => 'firstName',
                'label' => 'user',
                'value' => 'user.firstName'
            ],
            [
                'attribute' => 'name',
                'label' => 'Meal',
                'value' => 'meal.name'
            ],
            [
                'attribute' => 'categoryTypeName',
                'label' => 'categoryTypeName',
                'value' => 'categoryType.categoryTypeName'
            ],
            [
                'attribute' => 'categoryName',
                'label' => 'category Name',
                'value' => 'category.categoryName'
            ],
            
            [
                'attribute' => 'shiftName',
                'format' => 'ntext',
                'label' => 'shift Name',
                'value' => 'shift.shiftName'
            ],
              
            [
                'label' => 'Vendor',
                'format' => 'ntext',
                'attribute' => 'vendorName',
                'value' => function($model) {
                    foreach ($model->vendor as $vendor) {
                        $vendorNames[] = $vendor->vendorName;
                    }
                    return implode("\n", $vendorNames);
                },
            ],
            //'mealId',
            'orderDate',
            //'lastUpdate',
            // 'shift',
            // 'categoryTypeId',
            // 'categoryId',
            // 'status',
            // 'comment',
            ['class' => 'yii\grid\ActionColumn'],
        ],
                    */
    ]);
    ?>
</div>
<script type="text/javascript">
function apply_filter() {
    $('.grid-view').yiiGridView('applyFilter');
}
</script>