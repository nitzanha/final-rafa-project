<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Order */

$this->title = 'Display Order';
$this->params['breadcrumbs'][] = ['label' => 'Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-display">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('displayOrders', [	
        'model' => $model,
    ]) ?>

</div>
