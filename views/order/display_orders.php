<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use app\models\Order;
use app\models\User;
use yii\helpers\Url;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Order */
/* @var $form yii\widgets\ActiveForm */
$userID = Yii::$app->user->getId();
$get_orderURL = Url::to(['order/get_order'], true);
$orderCreateURL = Url::to(['order/create'], true);
$orderUpdateURL = Url::to(['order/update'], true);
$orderDetaisURL = Url::to(['order/order_detailes'], true);
$valideOrderUpdateURL = Url::to(['order/can_update_order'], true);
$ordergetEmployeeOrdersURL = Url::to(['order/get_employee_orders'], true);
?>
<div class="col-sm-4">
    <?php
    if (\Yii::$app->user->can('admin'))
    {
        echo Select2::widget([
            'id' => 'order-userid',
            'name' => 'state_2',
            'value' => '',
            'language' => 'he',
             'size' => Select2::LARGE,
            'data' => User::getEmployees(),
            'options' => ['dir' => 'rtl', 'placeholder' => 'בחר/י עובד']
            ]);
        //$userIsAdmin = 1;
    }
    else
    {
        echo '<div><h1>' . " " . "הזמנת ארוחות עבור " . Yii::$app->user->identity->username . "</h1> </div>";
        //$userIsAdmin = 0;
    }
    ?>
</div>
<div class="order-form">
<?php
$model = new Order();
$JSeventClick = <<<EOF
function(calEvent, jsEvent, view) {		
    var start = new Date(calEvent.start);  

    if (start <= new Date())
    {
        alert ("לא ניתן לערוך הזמנה מהעבר");
    }
    else
    {   
        var today = new Date();
        var dayDiff = dateDiffInDays(today.getFullYear() + "-" + (today.getMonth()+1) + "-" + today.getDate(), start);

        if (dayDiff == 0 && today.getHours() > "15")    //  can't order for today after 16:00
        {
            alert("מצטערים!" + "\\n" + "להיום ניתן לעדכן רק עד השעה 16:00.");
            throw new Error ("מצטערים!" + "\\n" + "להיום ניתן לעדכן רק עד השעה 16:00.");
        }

        if (new Date(start).getDay() == 5)    // if one of the selected days is Friday
        {
            if (dayDiff == 1)
            {
                if(today.getHours() > "15")
                {    
                    alert("מצטערים!" + "\\n" + "ליום שישי ניתן לעדכן עד יום חמישי ב 16:00.");
                    throw new Error ("מצטערים!" + "\\n" + "ליום שישי ניתן לעדכן עד יום חמישי ב 16:00.");
                }
            }
        }    
        
        //console.log("employeeID: " + employeeID);
        //if (employeeID == "undefined")
        //    employeeID = "";    
        /*
        var url = window.location.href;
        var res = url.split("/");  //split by space
        res.pop();  //remove last element
        url = res.join("/") + "/order/update";
        */
    
        var url = "$valideOrderUpdateURL";
        url += '?id=' + calEvent.id;

        jQuery.ajax({
            type: 'post',
            url: url,
            success: function (response) {
                response = removeHeader(response);
                if (response == 1)
                {
                    var employeeID = $('#order-userid').val();
                    var url = "$orderUpdateURL";
                    url += '?id=' + calEvent.id + "&employeeID=" + employeeID;

                    window.open(url, "_self");
                }
                else
                {    
                    alert("מצטערים!" + "\\n" + " לא ניתן כבר לעדכן מנה זו למשמרת זו");
                    throw new Error ("מצטערים!" + "\\n" + " לא ניתן כבר לעדכן מנה זו למשמרת זו");
                }
            },
            error: function () {
                console.log('failure');
            }
        });   
    }
}
EOF;
$JSdayClick = <<<EOF
function(date, allDay, jsEvent, view) {		
    var date = new Date(date);     
    /*
    var url = window.location.href;
    var res = url.split("/");  //split by space
    res.pop();  //remove last element
    url = res.join("/") + "/order/create";
    */
    var url = "$orderCreateURL";
    
    window.open(url, "_self");		
}
EOF;
$JSdayRender = <<<EOF
function (date, cell) {	
    var date = new Date(date);     
    
    if (date.getDay() != 6) // don't display checkbox on saturday
    {
        var year = date.getFullYear();
        var month = date.getMonth() + 1;
        if (month < 10)
            month = "0" + month;
        var day = date.getDate();
        if (day < 10)
            day = "0" + day;
        var formatDate = year + '-' + month + '-' + day;



        var unformatDate = year + "" + month + "" + day;
        /*
        var url = window.location.href;
        var res = url.split("/");  //split by space
        res.pop();  //remove last element
        url = res.join("/") + "/order/get_order";
        */
        var url = "$get_orderURL";
        
        var userID = $("#order-userid").val();
        //alert("val userID: " + userID);
        if (userID == "" || userID == null)
        {
            userID = $userID;
        //    alert("userID: " + userID);
        }
        //alert("userID: " + userID);
        
        jQuery.ajax({
            type: 'post',
            data: {date: formatDate, userID: userID},
            url: url,
            success: function (response) {
                response = removeHeader(response);
                /*
                console.log( "date: " + formatDate +
                    " response: " + response + 
                        " future: " + $(cell).hasClass("fc-future")  +
                        " today: " + $(cell).hasClass("fc-today") +
                        " other-month: " + !$(cell).hasClass("fc-other-month")
               );
               */ 
                //if ((response == "0") && (($(cell).hasClass("fc-future")) || ($(cell).hasClass("fc-today"))) && (!$(cell).hasClass("fc-other-month")))
                if ((($(cell).hasClass("fc-future")) || ($(cell).hasClass("fc-today"))) && (!$(cell).hasClass("fc-other-month")))
                {
                    $('.fc-future[data-date="' + formatDate + '"]').children("input").remove();
                    $('.fc-future[data-date="' + formatDate + '"]').append('<input class="checkbox" type="checkbox" name="selectedDay" id="daySelect_' + unformatDate 
                        + '">');
                    if (date.getDay() != 5) // don't display checkbox on Friday
                    {
                        $('.fc-today[data-date="' + formatDate + '"]').append('<input class="checkbox" type="checkbox" name="selectedDay" id="daySelect_' + unformatDate 
                            + '">');
                    }
                }
                /*
                if (response == "1")
                    cell.prepend('<img id="copyDay" src="/rafafoodproject/images/copy.png" />');
                */
            },
            error: function () {
                console.log('failure');
            }
        });
    }
}
EOF;
/*
$JSafterRender = <<<EOF
function( event, element, view ) {
    alert(event);
    alert(element);
    alert(view);
}
EOF;
 * 
 */
$JSeventMouseover = <<<EOF
	function(calEvent, jsEvent, view) {
        	$(this).css( { backgroundRepeat: "no-repeat", backgroundPosition: "right top", backgroundImage: "url(/rafafoodproject/images/copy.png)" } );  
        	// - Not working $(this).addClass("cancel_dish");
        }               			

EOF;
$JSafterAllRender = <<<EOF
function( view ) {
}
EOF;
?>
<?php

if (\Yii::$app->user->can('admin'))
    $events = array();
else
{
    $orders = Order::getUserOrders(Yii::$app->user->getId());
    
    $events = array();
    foreach ($orders as $key => $value)
    {
        $Event = new \yii2fullcalendar\models\Event();
        $Event->id = $value['key'];
        $Event->title = $value['title'];
        $Event->start = $value['start'];
        $events[] = $Event;
    }  
}
  
  ?>
  <?=
    yii2fullcalendar\yii2fullcalendar::widget(array(
        'clientOptions' => [
            'selectable' => true,		            
            'selectHelper' => true,
            'editable' => true,
            'displayEventTime' => false,
            'dayRender' => new JsExpression($JSdayRender),            
            'eventClick' => new JsExpression($JSeventClick),
            //'eventMouseover' => new JsExpression($JSeventMouseover),
            //'eventAfterRender' => new JsExpression($JSeventClick),
            //'dayClick' => new JsExpression($JSdayClick),
            //'eventAfterAllRender' => new JsExpression($JSafterRender),

            'lang' => 'he',			
            'isRTL' => true,
            //'defaultView' => 'basicWeek',			
        ],  
        'header'=> [
                'left' => 'next, prev, today',
                'center' => 'title',
                'right' => '',//'basicWeek', //'month, basicWeek, basicDay',
        ],
        /*
        'defaultView' => 'week',
        'views' => [
            'week' => [
                'type' => 'basic', 
                'duration' => [ 'weeks' => 2 ],
            ],
        ],
        */
        'events'=> $events,
    ));  
 ?>
    <div class="form-group">
        <?= Html::submitButton('Create', ['id' => 'orderSubmit', 'class' => 'btn btn-success']) ?>
        <?= Html::Button('Two Weeks', ['id' => 'copySubmit', 'class' => 'btn btn-success']) ?>
    </div>    
</div> 
<script>
    $(document).ready(function () {                
        $("#orderSubmit").on("click", function(e){
            var dates = [];
            $.each($("input[name='selectedDay']:checked"), function(){ 
                var id = $(this).attr("id");
                id = id.replace("daySelect_", "");
                id = id.substring(0, 4) + "-" + id.substring(4, 6) + "-" + id.substring(6); 

                var today = new Date();
                var dayDiff = dateDiffInDays(today.getFullYear() + "-" + (today.getMonth()+1) + "-" + today.getDate(), id);
                
                if (dayDiff == 0 && today.getHours() > "15")    //  can't order for today after 16:00
                {
                    alert("מצטערים!\nלהיום ניתן להזמין רק אחרי השעה 16:00.\nאנא בחר ימים לביצוע הזמנה שנית.");
                    $("input:checkbox").prop('checked', false); 
                    throw new Error ("מצטערים!\nלהיום ניתן להזמין רק אחרי השעה 16:00.\nאנא בחר ימים לביצוע הזמנה שנית.");
                }

                if (new Date(id).getDay() == 5)    // if one of the selected days is Friday
                {
                    if (dayDiff == 1)
                    {
                        if(today.getHours() > "15")
                        {    
                            alert("מצטערים!\nליום שישי יש להזמין עד יום חמישי ב 16:00.\nאנא בחר ימים לביצוע הזמנה שנית.");
                            $("input:checkbox").prop('checked', false); 
                            throw new Error ("מצטערים!\nליום שישי יש להזמין עד יום חמישי ב 16:00.\nאנא בחר ימים לביצוע הזמנה שנית.");
                        }
                    }
                }
                dates.push(id);
            });
            if (dates.length > 0)
            {
                /*
                var url = window.location.href;
                var res = url.split("/");  //split by space
                res.pop();  //remove last element
                url = res.join("/") + "/order/create";
                */
               
                var employeeID = $('#order-userid').val();
                //alert("employeeID" + employeeID);
                var url = '<?php echo $orderCreateURL; ?>';
                window.location.href = url + '?dates=' + dates + "&employeeID=" + employeeID;
            }
        });	             
        
        $("#copySubmit").on("click", function(e){
            var dates = [];
            $.each($("input[name='selectedDay']:checked"), function(){ 
                var id = $(this).attr("id");
                id = id.replace("daySelect_", "");
                id = id.substring(0, 4) + "-" + id.substring(4, 6) + "-" + id.substring(6); 
                dates.push(id);
            });
            if (dates.length > 1)
            {
                alert ("ניתן להעתיק רק יום אחד");
            }
        });
        
        $("#order-userid").on("change", function(e) {
            $('.fc-future').children("input").remove();
            $('.table').find('img').remove();
            $('#w0').fullCalendar( 'removeEvents' );
            //alert($('#order-userid').val());
            jQuery.ajax({
                type: 'post',
                cache: false,
                data: {userID: $('#order-userid').val()},
                url: '<?php echo $ordergetEmployeeOrdersURL; ?>',
                dataType: 'html',
                success: function (response) {
                    //console.log("response: " + response + "DDD");
                    response = removeHeader(response);
                    var myArray = JSON.parse(response);
                    //console.log("response: " + response);

                    $('#w0').fullCalendar( 'removeEvents' );
                    $('#w0').fullCalendar( 'addEventSource', myArray );
                    $('#w0').fullCalendar('prev');
                    $('#w0').fullCalendar('next');
                },
                error: function(requestObject, error, errorThrown) {
                    //console.log (requestObject);
                    //console.log(error);
                    //console.log(errorThrown);
                    console.log('failure order-userid change');
                    $('#w0').fullCalendar( 'removeEvents' );
                    $('#w0').fullCalendar('prev');
                    $('#w0').fullCalendar('next');
                }
            });
        });
    });
    
        // after order update
        var employeeID = getParameterByName('employeeID');
        console.log("employeeID" + employeeID + "*");
        if (employeeID == -1)
            employeeID = $userID;
        //if (employeeID !== -1 && employeeID != "undefined")
        //{
            console.log("employeeIDX" + employeeID + "*");
            alert("GG");
            jQuery.ajax({
                type: 'get',
                cache: false,
                data: {userID: employeeID},
                url: '<?php echo $ordergetEmployeeOrdersURL; ?>',
                dataType: 'html',
                success: function (response) {
                    //console.log("response: " + response);
                    $("#order-userid").val(employeeID).trigger('change'); 
                    response = removeHeader(response);
                    var myArray = JSON.parse(response);
                    console.log("response: " + response);
                    $('#w0').fullCalendar( 'removeEvents' );
                    $('#w0').fullCalendar( 'addEventSource', myArray );
                    $('#w0').fullCalendar('prev');
                    $('#w0').fullCalendar('next');
                    removeQueryStringParameter('employeeID');
                },
                error: function () {
                    console.log('failure in after order update');
                }
            });        
        //}        
            
    function removeHeader(response) {
        //console.log('response: *' + response + '*');
        var n = response.indexOf("!@#");
        var response = response.substr(n + 3);
       // console.log('response: ^' + response + '^');

        var n = response.indexOf("!@#");
        var response = response.substr(0, n);

        return response;
    }   
    
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return -1;
    if (!results[2]) return -1;
    if (results[2] == "undefined") return -1;
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}    

function removeQueryStringParameter(key, url) {
    if (!url) url = window.location.href;

    var hashParts = url.split('#');

    var regex = new RegExp("([?&])" + key + "=.*?(&|#|$)", "i");

    if (hashParts[0].match(regex)) {
        //REMOVE KEY AND VALUE
        url = hashParts[0].replace(regex, '$1');

        //REMOVE TRAILING ? OR &
        url = url.replace(/([?&])$/, '');

        //ADD HASH
        if (typeof hashParts[1] !== 'undefined' && hashParts[1] !== null)
            url += '#' + hashParts[1];
    }

    return url;
}

function dateDiffInDays(date1, date2) {
    dt1 = new Date(date1);
    dt2 = new Date(date2);
    return Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), 
        dt1.getDate()) ) /(1000 * 60 * 60 * 24));
}                    
</script>
