<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use app\models\Shift;
use app\models\Categorytype;
use app\models\Category;
//use kartik\radioButtonGroup;
//use kartik\widgets\radioButtonGroup;
use kartik\form\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Order */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-form">

    <?php $form = ActiveForm::begin(); ?>


    <h2>הזמנה לתאריך <?php echo $model->orderDate; ?></h2>
    
    <?= //$form->field($model, 'shift')->radioButtonGroup(shift::getShifts(), [
        $form->field($model, 'shift')->radioButtonGroup(Shift::getShiftsForUpdate(), [
        'class' => 'btn-group-sm',
        'itemOptions' => ['labelOptions' => ['class' => 'btn btn-warning']]
        ]);
    ?>
  
    <?= //$form->field($model, 'categoryTypeId')->radioButtonGroup(categorytype::getCategorytypes(), [
        $form->field($model, 'categoryTypeId')->radioButtonGroup(categorytype::getCategorytypesByOrder(), [
        'class' => 'btn-group-sm',
        'itemOptions' => ['labelOptions' => ['class' => 'btn btn-warning']]
        ]);
    ?>    
    
    <?= $form->field($model, 'categoryId')->radioButtonGroup(category::getCategories(), [
        'id' => 'categoryType',
        'itemOptions' => ['labelOptions' => ['class' => 'btn btn-warning']]
        ]);
    ?>     

    <div id='mealID'>
        <div class="form-group field-order-mealId required">
            <label class="control-label">meals</label>
            <input type="hidden" name="Order[mealId]" id="orderMealID" value="">
            <div id="order-meal" aria-required="true">
                <table id="mealTable" class="table table-striped table-bordered table-sm table-responsive">
                    <thead class="thead-dark">
                        <tr>
                            <th id="ID">ID</th>
                            <th>name</th>
                            <th>תוספות</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
                <div class="help-block"></div>
            </div>	
        </div>
    </div>    
    
    
	
    <!--  $form->field($model, 'userId')->textInput() -->

    <!--  $form->field($model, 'mealId')->textInput() -->

    <!--<?= $form->field($model, 'orderDate')->textInput() ?>-->

    <!--  $form->field($model, 'lastUpdate')->textInput() -->
	
	<?= $form->field($model, 'comment')->textarea (array('maxlength' => true, 'rows' => 6, 'cols' => 50)); ?> 

    <!-- $form->field($model, 'status')->textInput() -->



    <div class="form-group">
        <?= Html::submitButton('Update', ['id' => 'updateSubmit', 'class' => 'btn btn-primary']) ?>        
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </div>       
<script>
    var orderID = <?php echo $model->id; ?>;
    var userID = <?php echo $model->userId; ?>;
    var mealID = <?php echo $model->mealId; ?>;    
    var orderDate = <?php echo $model->orderDate; ?>;
    var lastUpdate = <?php echo $model->lastUpdate; ?>;
    var shift = <?php echo $model->shift; ?>;
    var categoryTypeId = <?php echo $model->categoryTypeId; ?>;
    var categoryID = <?php echo $model->categoryId; ?>;    
    var status = <?php echo $model->status; ?>;
    //var comment = <?php echo $model->comment; ?>;       
 
    var url = window.location.href;
    var res = url.split("/");  //split by space
    res.pop();  //remove last element
    res.pop();
    //url = res.join("/") + "/meal/get_meals_by_type_id";
    url = res.join("/") + "/meal/get_meals_by_parameters";

    console.log("shift: "  + shift + " categoryType: " + categoryTypeId + " mealType: " + categoryID);
    jQuery.ajax({
        type: 'get',
        //cache: false,
        //data: {mealType: categoryID},
        data: {shift: shift, categoryType: categoryTypeId, mealType: categoryID},
        url: url,
        success: function (response) {
            response = removeHeader(response);
            $('#mealTable').append(response);
            $('#mealID').show();
            var table = document.getElementById("mealTable");
            for (var i = 0, row; row = table.rows[i]; i++) {
               //iterate through rows
               //rows would be accessed using the "row" variable assigned in the for loop
               for (var j = 0, col; col = row.cells[j]; j++) {
                   console.log (row.cells[0].innerHTML + " HH " + mealID);
                   if (row.cells[0].innerHTML == mealID)
                   {
                       var rowNum = i -1;
                       $('#mealTable tbody tr:eq(' + rowNum + ')').css('background','red');
                       break;
                   }
                 //iterate through columns
                 //columns would be accessed using the "col" variable assigned in the for loop
               }  
            }            
/*
            $('button[id^="mealsID"]').on('click', function() {  
                mealID = $(this).attr('value'); // Get the value attribute
                $('#orderMealID').val(mealID);
            });
*/        
        },
        error: function () {
            console.log('failure');
        }
    });     
        $(document).ready(function() {           
            $('#order-shift').change(function () {
                $('#order-shift').find('input:checked').each(function() {
                    shift = $(this).val();
                });                 
                
                $('.field-order-categorytypeid').hide();
                $('.field-categoryType').hide();
                $('#mealID').hide(); 

                var url = window.location.href;
                var res = url.split("/");  //split by space
                res.pop();  //remove last element
                res.pop();
                url = res.join("/") + "/categorytype/get_category_for_update";
              //  alert("shift" + shift + "^^" + $(this).attr('value') + "GG" + document.getElementsByName("Order[shift]").value);
                jQuery.ajax({
                    type: 'get',
                    url: url,
                    data: {shift: shift},
                    success: function (response) {
                        response = removeHeader(response);
                        console.log("response: " + response);
                        $('#order-categorytypeid').html(response);
                        $('.field-order-categorytypeid').show();

                        $('button[id^="categoryTypeID"]').on('click', function() {  
                            categoryTypeID = $(this).attr('value'); // Get the value attribute
                            $('#orderCategoryTypeID').val(categoryTypeID);
                        });
                        //$('#categoryTypeID').show();
                        //$('#mealTypeID').show();
                        //return false;
                    },
                    error: function () {
                        console.log('failure');
                    }
                });
            });              
            
            //$('#updateSubmit').attr('disabled','disabled');
            $('#order-categorytypeid').on('change', function() {  
                $('#order-categorytypeid  > .active').find('input:checked').each(function() {
                     categoryTypeId = $(this).val();
                }); 
        
                $('.field-categoryType').hide();
                $('#mealID').hide();  
                
                var url = window.location.href;
                var res = url.split("/");  //split by space
                res.pop();  //remove last element
                res.pop();
                //url = res.join("/") + "/category/get_meals";
                url = res.join("/") + "/category/get_meals_by_parameters_for_update";

                jQuery.ajax({
                    type: 'post',
                    //data: {categoryType: categoryTypeId},
                    data: {shift: shift, categoryType: categoryTypeId},
                    url: url,
                    success: function (response) {
                        response = removeHeader(response);
                        $('#categoryType').html(response);
                        $('.field-categoryType').show();
                        document.getElementsByName("Order[categoryTypeId]").value = categoryTypeId;
                    },
                    error: function () {
                        console.log('failure');
                    }
                });                  
            });
            
            $('#categoryType').on('change', function() {  
                $('#categoryType  > .active').find('input:checked').each(function() {
                     categoryID = $(this).val();
                });               

                if (document.getElementById("mealTable").rows.length > 1)
                {
                    for (var i = document.getElementById("mealTable").rows.length; i > 1;i--)
                    {
                        console.log("i" + i);
                        document.getElementById("mealTable").deleteRow(i - 1);
                    }
                }
                
                var url = window.location.href;
                var res = url.split("/");  //split by space
                res.pop();  //remove last element
                res.pop();
                //url = res.join("/") + "/meal/get_meals_by_type_id";
                url = res.join("/") + "/meal/get_meals_by_parameters";

                jQuery.ajax({
                    type: 'post',
                    cache: false,
                    //data: {mealType: categoryID},
                    data: {shift: shift, categoryType: categoryTypeId, mealType: categoryID},
                    url: url,
                    success: function (response) {
                        response = removeHeader(response);
                        $('#mealTable').append(response);
                        $('#mealID').show();
                        document.getElementsByName("Order[categoryType]").value = categoryID;
                    },
                    error: function () {
                        console.log('failure');
                    }
                });     
            });            

            $("#mealTable").on('click','td',function(){
                $("#mealTable tr").css('background', '');
                 // get the current row
                var currentRow=$(this).closest("tr"); 

                var col1=currentRow.find("td:eq(0)").text(); // get current row 1st TD value

                currentRow.css('background', 'red');
                mealID = col1;
                document.getElementsByName("Order[mealId]").value = mealID;
            });
            
            $('#updateSubmit').click(function () {
                var url = window.location.href;            
                //var dates = getParameterByName('dates');
                
                var employeeID = getParameterByName('employeeID');
                var mealExtras = $('#mealExtras').val();
                //console.log("employeeID: " + employeeID);
                //if (employeeID == "undefined")
                //    employeeID = "";
                
                console.log("employeeID: " + employeeID);

                var res = url.split("/");  //split by space
                res.pop();  //remove last element
                res.pop();
                url = res.join("/") + "/order/update";

                jQuery.ajax({
                    type: 'get',
                    //cache: false,
                    data: {id: orderID, mealID: mealID, orderCategoryID: categoryID, shift: shift, categoryTypeId: categoryTypeId, 
                        comment: $('#order-comment').val(), employeeID: employeeID, mealExtras: mealExtras},
                    url: url,
                    success: function (response) {
                        console.log('success');
                        var url = window.location.href;            
                        var res = url.split("/");  //split by space
                        res.pop();  //remove last element
                        res.pop();
                        url = res.join("/") + "/order/display_orders?employeeID=" + employeeID;
                        window.open(url, "_self");
                    },
                    error: function () {
                        console.log('failure');
                    }
                }); 
            });            
        });
        


    function removeHeader(response) {
        //console.log('response: *' + response + '*');
        var n = response.indexOf("!@#");
        var response = response.substr(n + 3);
       // console.log('response: ^' + response + '^');

        var n = response.indexOf("!@#");
        var response = response.substr(0, n);

        return response;
    }   
    
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}    
</script>