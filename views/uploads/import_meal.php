<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
//use app\models\Category;
//use app\models\Vendor;
//use app\models\Meal;
//use kartik\widgets\FileInput;
//use kartik\FileInput;
use \kartik\file\FileInput;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Meal */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="meal-form">

<?php
$form = ActiveForm::begin([
   'options' => ['enctype' => 'multipart/form-data'] // important
]);
?>

<?=

    $form->field($model, 'filename')->widget(FileInput::classname(), [
        'options' => ['accept' => 'filename/*'],
        'pluginOptions' => ['allowedFileExtensions' => ['xls'], 'showUpload' => true,]
    ]);
/*
    FileInput::widget([
        'name' => 'filename',
        'options' => ['accept' => 'file'],
        'pluginOptions' => [
            'showUpload' => true,
            'showPreview' => false,
            'browseLabel' => 'בחר קובץ',
            'language' => 'he',
            'removeLabel' => '',
            'allowedFileExtensions' => ['xls'],
            'mainClass' => 'input-group-md'
        ]
    ]);
 * 
 */
?>    

<?php ActiveForm::end(); ?>


</div>
