<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'languagepicker'],
	'homeUrl' => ['site/index'],
	'language' => 'en-US',
	'modules' => [
	'dynagrid'=>[
        'class'=>'\kartik\dynagrid\Module',
        // other settings (refer documentation)
    ],
   'gridview' =>  [
        'class' => '\kartik\grid\Module'],
	
    ],
    'timeZone' => 'Asia/Jerusalem', 
    'components' => [
	    'assetManager' => [
    	    'bundles' => [
    	        'yii\web\JqueryAsset' => [
    	            'jsOptions' => [ 'position' => \yii\web\View::POS_HEAD ],
    	        ],
    	    ],
    	],	
        'languagepicker' => [
                'class' => 'lajax\languagepicker\Component',
                'languages' => ['en-US', 'he'],         // List of available languages (icons only)
                'cookieName' => 'language',                         // Name of the cookie.
                'expireDays' => 64,                                 // The expiration time of the cookie is 64 days.
                'callback' => function() {

                        if (Yii::$app->user->isGuest) {
                        }
                }
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'nitzanha',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
            //'on '.\yii\web\User::EVENT_AFTER_LOGIN => [$this, 'display_orders'], 
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => true,
                        //'suffix' => '.html',
                        //'cache' => 'cache',
                        //'scriptUrl' => '',
                        'baseUrl' => '/',
                        //'hostInfo' => 'http://www.yourhost.com.au',
                        //'routeParam' => 'r',
                        'ruleConfig' => [
                                'class' => 'yii\web\UrlRule'
                        ],
                        'rules' => array(
								
                                [
                                        'pattern' => 'gii',
                                        'suffix' => '',
                                        'route' => 'gii',
                                ],
								/*
                                [
                                        'pattern' => '/<controller:\w+>',
                                        'suffix' => '',
                                        'route' => 'gii/<controller>',
                                ],
                                [
                                        'pattern' => '/<controller:\w+>/<action:\w+>',
                                        'suffix' => '',
                                        'route' => 'gii/<controller>/<action>',
                                ],
								*/
                                '' => 'site/index',
                                [
                                        'pattern' => '<action:\w+>',
                                        'suffix' => '.html',
                                        'route' => 'site/<action>',
                                ],
                               
                        ),
        ],

		
			'authManager' => [
			'class' => 'yii\rbac\DbManager',
				],
				
		
		
    ],

    'params' => $params,
//	 'icon-framework' => 'fa',
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
       'allowedIPs' => ['*'],
    ];
}

return $config;
