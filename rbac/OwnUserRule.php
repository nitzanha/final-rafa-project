<?php
namespace app\rbac;

use yii\rbac\Rule;
use Yii; 
//המחלקה חייבת לרשת את מחלקה חוק
class OwnUserRule extends Rule
{
	//חייבת להיות הגדרה של שם החוק
	public $name = 'ownUserRule';
	
	//הגדרה מהו היוזר הרלוונטי, איזו הרשאה החוק הזה צמוד אליה,האחרון זה מערך של פרמטרים שאנו יכולים להעביר מחוץ לחוק
	public function execute($user, $item, $params)
	{
		//בדיקה שהיוזר רשום - login
		if (!Yii::$app->user->isGuest) {
			//בדיקה שהיוזר שייך לאותו owner
			return isset($params['user']) ? $params['user']->id == $user : false;
		}
		return false;
	}
}