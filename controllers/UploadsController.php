<?php

namespace app\controllers;

use Yii;
use app\models\Uploads;
use app\models\UploadsSearch;
//use app\models\Meal;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * UploadsController implements the CRUD actions for Uploads model.
 */
class UploadsController extends Controller
{
    public function beforeAction($action) {
        if (Yii::$app->language == "he") {
            ?>
            <link href="/rafafoodproject/web/assets/8b0376e8/css/bootstrap.css" rel="stylesheet">
            <link href="/rafafoodproject/web/assets/8b0376e8/css/bootstrap-rtl.css" rel="stylesheet">
            <?php
        } else {
            ?>
            <link href="/rafafoodproject/web/assets/8b0376e8/css/bootstrap.css" rel="stylesheet">
            <?php
        }

        return parent::beforeAction($action);
    }    
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Uploads models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UploadsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Uploads model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Uploads model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Uploads();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->uploadID]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Uploads model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->uploadID]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Uploads model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Uploads model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Uploads the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Uploads::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionImport_meal() {
        $model = new Uploads();
        if ($model->load(Yii::$app->request->post())) 
        {
            $file = UploadedFile::getInstance($model, 'filename');
            if (!is_null($file)) {
                $model->file_src_filename = $file->name;
                $ext = end((explode(".", $file->name)));
                // generate a unique file name to prevent duplicate filenames
                $model->file_web_filename = Yii::$app->security->generateRandomString() . ".{$ext}";
                // the path to save file, you can set an uploadPath
                // in Yii::$app->params (as used in example below)                       
                Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/web/uploads/meals/';
                $path = Yii::$app->params['uploadPath'] . $model->file_web_filename;
                $file->saveAs($path);
            }
            if ($model->save()) {
                $inputFile = $path;
                try {
                    $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
                    $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel = $objReader->load($inputFile);
                } catch (Exception $ex) {
                    die($ex);
                }
                $sheet = $objPHPExcel->getSheet(0);
                $highstRow = $sheet->getHighestRow();
                $highstColumn = $sheet->getHighestColumn();
                
                for( $row = 1; $row <= $highstRow; $row++)
                {
                    $rowData = $sheet->rangeToArray('A'.$row.':'.$highstColumn.$row, NULL, TRUE, FALSE);
                    
                    if ($row == 1)
                    {
                        continue;
                    }
                    else
                    {
                        $modelMeal = new \app\models\Meal();
                        $modelMeal->name = $rowData[0][0];
                        $modelMeal->details = $rowData[0][1];
                        $modelMeal->vendorId = $rowData[0][2];
                        $modelMeal->categoryId = $rowData[0][3];
                        $modelMeal->picture = $rowData[0][4];
                        $modelMeal->status = $rowData[0][5];
                        $modelMeal->comment = $rowData[0][6];
                        $modelMeal->mealShift = $rowData[0][7];
                        $modelMeal->mealCategoryType = $rowData[0][8];
                        $modelMeal->save();
                            //,image_src_filename,image_web_filename
                    }
                }
                return $this->redirect(['/meal/index']);
            } else {
                var_dump($model->getErrors());
                //file_put_contents("/var/www/html/testYair.log", "ERROR: " . print_r($model->getErrors, true) . "\n", FILE_APPEND);
                die();
            }
        }
        return $this->render('import_meal', [
                'model' => $model,
        ]);
    }     
}
