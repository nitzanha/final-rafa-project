<?php

namespace app\controllers;

use Yii;
use app\models\Categorytype;
//use app\models\CategoryTypeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CategoryTypeController implements the CRUD actions for CategoryType model.
 */
class CategorytypeController extends Controller
{    
    public function beforeAction($action) {
        if (Yii::$app->language == "he") 
        {
        ?>
            <link href="/rafafoodproject/web/assets/8b0376e8/css/bootstrap.css" rel="stylesheet">
            <link href="/rafafoodproject/web/assets/8b0376e8/css/bootstrap-rtl.css" rel="stylesheet">
        <?php
        } 
        else 
        {
        ?>
            <link href="/rafafoodproject/web/assets/8b0376e8/css/bootstrap.css" rel="stylesheet">
        <?php
        }

        return parent::beforeAction($action);
    }    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CategoryType models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CategoryTypeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CategoryType model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CategoryType model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CategoryType();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing CategoryType model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing CategoryType model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CategoryType model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CategoryType the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CategoryType::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    public function actionGet_category()
    {
        $types = categorytype::getCategorytypesByShift($_GET['shift']);
        $buttons = "";

       // die("DDD");
             
        foreach ($types as $key => $value)
        {
            $buttons .= '<button class"btn" id="categoryTypeID_' . $key . '" name="Order[categoryTypeId]" value="' . $key . '">  <span>' . $value . '</span>  </button>';
            //$buttons .= '<button class"btn" id="shiftID_' . $key . '" name="Order[shift]" value="' . $key . '">  <span>' . $value . '</span>  </button>';
        }      
        echo "!@#" . $buttons . "!@#";
    }   
    
    public function actionGet_category_for_update()
    {
        $types = categorytype::getCategorytypesByShift($_GET['shift']);
        $buttons = "";
        $index = 0;
        
        foreach ($types as $key => $value)
        {
           // $buttons .= '<button class"btn" id="categoryTypeID_' . $key . '" name="Order[categoryTypeId]" value="' . $key . '">  <span>' . $value . '</span>  </button>';
            $buttons .= '<label class="btn btn-warning active"><input type="radio" name="Order[categoryTypeId]" value="' . $key . '" data-index="' .
                $index . '">' . $value . '</label>';
            $index++;
        }      
        echo "!@#" . $buttons . "!@#";
    }    
    
    public function actionGet_all_categoris()
    {
        $types = categorytype::getCategorytypes();
        $buttons = "";
        
        foreach ($types as $key => $value)
        {
            $buttons .= '<button class"btn" id="categoryTypeID_' . $key . '" name="Order[categoryTypeId]" value="' . $key . '">  <span>' . $value . '</span>  </button>';
            //$buttons .= '<button class"btn" id="shiftID_' . $key . '" name="Order[shift]" value="' . $key . '">  <span>' . $value . '</span>  </button>';
        }      
        echo "!@#" . $buttons . "!@#";
    }     
}