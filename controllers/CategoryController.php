<?php

namespace app\controllers;

use Yii;
use app\models\Category;
use app\models\CategorySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * CategoryController implements the CRUD actions for Category model.
 */
class CategoryController extends Controller {

    public function beforeAction($action) {
        if (Yii::$app->language == "he") {
            ?>
            <link href="/rafafoodproject/web/assets/8b0376e8/css/bootstrap.css" rel="stylesheet">
            <link href="/rafafoodproject/web/assets/8b0376e8/css/bootstrap-rtl.css" rel="stylesheet">
            <?php
        } else {
            ?>
            <link href="/rafafoodproject/web/assets/8b0376e8/css/bootstrap.css" rel="stylesheet">
            <?php
        }

        return parent::beforeAction($action);
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Category models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new CategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Category model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Category model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Category();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                    'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Category model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                    'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Category model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Category model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Category the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Category::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionGet_meals() {
        if (Yii::$app->request->isAjax) {
            $data = Yii::$app->request->post();
            $categoryType = $data['categoryType'];

            $allCategories = Category::find()
                ->where(['categoryType' => $categoryType])
                ->all();

            $allCategoriesArray = ArrayHelper::
                map($allCategories, 'id', 'categoryName');

            $select = "";
            foreach ($allCategoriesArray as $key => $value) {
                $select .= '<button type="button" id="orderCategoryID_' . $key . '" class ="mealCategoryID" name="Order[categoryId]" value="' . $key . '">' . $value;
            }
        }
        return "!@#" . $select . "!@#";
    }

    public function actionGet_meals_by_parameters() {
        if (Yii::$app->request->isAjax) {
            $data = Yii::$app->request->post();
            $shift = $data['shift'];
            $categoryType = $data['categoryType'];

            $categorySql = (new \yii\db\Query())
                ->select(['category.id', 'category.categoryName'])
                ->distinct()
                ->from('meal')
                ->where(['mealShift' => $shift, 'mealCategoryType' => $categoryType])
                ->join('INNER JOIN', 'category', 'category.id = meal.categoryId')
                ->createCommand();
            
        //file_put_contents("/var/www/html/testYair.log", "sql: " . print_r($categorySql->sql, true) . "\n", FILE_APPEND);
        //file_put_contents("/var/www/html/testYair.log", "params: " . print_r($categorySql->params, true) . "\n", FILE_APPEND);

        $categoryRows = $categorySql->queryAll();
        //file_put_contents("/var/www/html/testYair.log", "mealRows: " . print_r($categoryRows, true) . "\n", FILE_APPEND);            

            $allCategoriesArray = ArrayHelper::
                map($categoryRows, 'id', 'categoryName');        
            /*
            $allCategories = Category::find()
                ->where(['categoryType' => $categoryType])
                ->all();

            $allCategoriesArray = ArrayHelper::
                map($allCategories, 'id', 'categoryName');
            */
            $select = "";
            foreach ($allCategoriesArray as $key => $value) {
                $select .= '<button type="button" id="orderCategoryID_' . $key . '" class ="mealCategoryID" name="Order[categoryId]" value="' . $key . '">' . $value;
            }
        }
        return "!@#" . $select . "!@#";
    }
    
    public function actionGet_meals_by_parameters_for_update() {
        if (Yii::$app->request->isAjax) {
            $data = Yii::$app->request->post();
            $shift = $data['shift'];
            $categoryType = $data['categoryType'];

            $categorySql = (new \yii\db\Query())
                ->select(['category.id', 'category.categoryName'])
                ->distinct()
                ->from('meal')
                ->where(['mealShift' => $shift, 'mealCategoryType' => $categoryType])
                ->join('INNER JOIN', 'category', 'category.id = meal.categoryId')
                ->createCommand();
            
            $categoryRows = $categorySql->queryAll();

            $allCategoriesArray = ArrayHelper::
                map($categoryRows, 'id', 'categoryName');        

            $select = "";
            $index = 0;
            foreach ($allCategoriesArray as $key => $value) {
                //$select .= '<button type="button" id="orderCategoryID_' . $key . '" class ="mealCategoryID" name="Order[categoryId]" value="' . $key . '">' . $value;
                $select .= '<label class="btn btn-warning active"><input type="radio" name="Order[categoryId]" value="' . $key . '" data-index="' .
                    $index . '">' . $value . '</label>';
                $index++;                
            }
        }
        return "!@#" . $select . "!@#";
    }    

    public function actionGet_meals_for_update() {
        if (Yii::$app->request->isAjax) {
            //$data = Yii::$app->request->post();
            //$categoryType = $data['categoryType'];
            if (Yii::$app->request->post()) {
                $data = Yii::$app->request->post();
                $categoryType = $data['categoryType'];
            } else {
                $data = Yii::$app->request->get();
                $categoryType = $data['categoryType'];
            }

            $allCategories = Category::find()
                ->where(['categoryType' => $categoryType])
                ->all();

            $allCategoriesArray = ArrayHelper::
                map($allCategories, 'id', 'categoryName');

            $select = "";
            $index = 0;
            foreach ($allCategoriesArray as $key => $value) {
                //$select .= '<button type="button" id="orderCategoryID_' . $key . '" class ="mealCategoryID" name="Order[categoryId]" value="' . $key . '">' . $value;
                $select .= '<label class="btn btn-warning active"><input type="radio" name="Order[categoryTypeId]" value="' . $key . '" data-index="' .
                    $index . '">' . $value . '</label>';
                $index++;
            }
        }
        return "!@#" . $select . "!@#";
    }

}
