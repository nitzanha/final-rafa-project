<?php

namespace app\controllers;

use Yii;
use app\models\Order;
use app\models\OrderSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Query;
use yii\helpers\Json;
use yii\data\SqlDataProvider;
//use yii\base\Event;
//use yii\web\Response;

/**
 * OrderController implements the CRUD actions for Order model.
 */
class OrderController extends Controller {

    public function beforeAction($action) {
        if (Yii::$app->language == "he") 
        {
        ?>
            <link href="/rafafoodproject/web/assets/8b0376e8/css/bootstrap.css" rel="stylesheet">
            <link href="/rafafoodproject/web/assets/8b0376e8/css/bootstrap-rtl.css" rel="stylesheet">
        <?php
        } 
        else 
        {
        ?>
            <link href="/rafafoodproject/web/assets/8b0376e8/css/bootstrap.css" rel="stylesheet">
        <?php
        }

        return parent::beforeAction($action);
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
               
        return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Order model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Order model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Order();
        
        if (Yii::$app->request->post()) 
        {
            $datesArray = explode(",", $_POST['dates']);

            for ($x = 0; $x < count($datesArray); $x++) {
                $model = new Order();
                //$model->attributes = Yii::$app->request->post('Order');

                if (!empty($_POST['mealExtras']))
                    $model->orderExtras = implode(",", $_POST['mealExtras']);
                
                if ($_POST['employeeID'] == "")
                    $model->userId = Yii::$app->user->getId();
                else
                    $model->userId = $_POST['employeeID']; 
                
                $model->mealId = $_POST['mealID'];
                $model->orderDate = $datesArray[$x];
                $model->lastUpdate = date('Y-m-d H:i:s');
                $model->shift = $_POST['shift'];
                $model->categoryTypeId = $_POST['categoryTypeId'];
                $model->categoryId = $_POST['orderCategoryID'];

                //$model->status = $_POST['Order']['status'];
                if ($_POST['comment'])
                    $model->comment = $_POST['comment'];
                try 
                {
                    if ($model->save(false))
                    {
                     //  file_put_contents("/var/www/html/testYair.log", "aa" . "\n" , FILE_APPEND); 
                        echo "SUCCESS ";
                        $this->render('display_orders', [
                            'model' => $model,
                        ]);
                    }
                    else
                    {
                        echo "FAILURE: ";
                    }
                } 
                catch (\Exception $e) 
                {
                    // throw new \yii\web\HttpException(405, 'שימוש ב ctrl + R אסור בנקודה זו');
                    Yii::$app->session->setFlash('success', 'שימוש ב ctrl + R אסור בנקודה זו');
                    echo "FAILURE: " . $e->getMessage() . "\n";
                   //file_put_contents("/var/www/html/testYair.log", "error: " . print_r($e->getMessage(), true) . "\n" , FILE_APPEND);
                    return $this->render('display_orders', [
                       'model' => $model,
                    ]);
                }
            }
            return $this->render('display_orders', [
                'model' => $model,
            ]);                
        }
        else 
        {
            echo "failure";
            return $this->render('create', [
                    'model' => $model,
            ]);
        }

        /*
          if ($model->load(Yii::$app->request->post()) && $model->save()) {
          return $this->redirect(['view', 'id' => $model->id]);
          } else {
          return $this->render('create', [
          'model' => $model,
          ]);
          }
         */
        return $this->render('display_orders', [
           'model' => $model,
        ]);
    }

    /**
     * Updates an existing Order model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    
    public function actionUpdate($id) {
    //public function actionUpdate() {    
        $model = $this->findModel($id);

        if (Yii::$app->request->isAjax) 
        { 
            
            if (!empty($_GET['mealExtras']))
                    $model->orderExtras = implode(",", $_GET['mealExtras']);
            
            if ($_GET['employeeID'] == "null" || $_GET['employeeID'] == "undefined")
                $model->userId = Yii::$app->user->getId();
            else
                $model->userId = $_GET['employeeID'];
            $model->mealId = $_GET['mealID'];
            //$model->orderDate = $_GET['orderDate'];
            $model->lastUpdate = date('Y-m-d H:i:s');
            $model->shift = $_GET['shift'];

            $model->categoryTypeId = $_GET['categoryTypeId'];
            $model->categoryId = $_GET['orderCategoryID'];
            //$model->status = $_POST['Order']['status'];
            if ($_GET['comment'])
                $model->comment = $_GET['comment'];
            try 
            {
                if ($model->save(false))
                {
                    //file_put_contents("/var/www/html/testYair.log", "aa" . "\n" , FILE_APPEND); 
                    echo "SUCCESS ";
                    $this->render('display_orders', [
                        'model' => $model,
                    ]);
                }
                else
                {
                    echo "FAILURE: ";
                }
            } 
            catch (\Exception $e) 
            {
                // throw new \yii\web\HxttpException(405, 'שימוש ב ctrl + R אסור בנקודה זו');
                Yii::$app->session->setFlash('success', 'שימוש ב ctrl + R אסור בנקודה זו');
                echo "FAILURE: " . $e->getMessage() . "\n";
                //file_put_contents("/var/www/html/testYair.log", "error: " . print_r($e->getMessage(), true) . "\n" , FILE_APPEND);
                return $this->render('display_orders', [
                   'model' => $model,
                ]);
            }

            return $this->render('display_orders', [
                'model' => $model,
            ]);                
        }
        else 
        {
            echo "failure";
            return $this->render('update', [
                'model' => $model,
            ]);
        }

        return $this->render('display_orders', [
            'model' => $model,
            'employee' => $model->userId,
        ]);

/* 
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            file_put_contents("/var/www/html/testYair.log", "in save: " . "\n" , FILE_APPEND); 
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                    'model' => $model,
            ]);
        }
*/
    }

    /**
     * Deletes an existing Order model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();
        $model = new Order();

        return $this->render('display_orders', [
            'model' => $model,
        ]);              
    }

    /**
     * Finds the Order model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Order the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionDisplay_orders() {
        $model = new Order();
        return $this->render('display_orders', [
            'model' => $model,
        ]);
    }

    public function actionGet_order() {    
        if (Yii::$app->request->isAjax) {
            $data = Yii::$app->request->post();
            $orderDate = $data['date'];
            $userID = $data['userID'];
            
            $sql = 'SELECT * FROM `order` WHERE userId = ' . $userID . ' AND orderDate = "' . $orderDate . '"';

            try
            {
                $connection = Yii::$app->db;
                $model = $connection->createCommand($sql);
                $rows = $model->queryOne();    
                
                if (empty($rows))
                   return "!@#" . 0 . "!@#";
                else
                    return "!@#" . 1 . "!@#";
            } 
            catch (\Exception $e) 
            {
                //file_put_contents("/var/www/html/testYair.log", "error " .  print_r($e->getMessage(), true) . "\n" , FILE_APPEND);
                print_r($e->getMessage());
                return "!@#" . 0 . "!@#";
            }  
        }
    }
    
    public function actionGet_employee_orders()
    {	//http://www.yiiframework.com/extension/yii2fullcalendar/
        if (Yii::$app->request->post())
        {
            $data = Yii::$app->request->post();
            $userID = $data['userID'];
        }
        else
        {
            $data = Yii::$app->request->get();
            $userID = $data['userID'];
        } 
        $events = array();
        if (!empty($userID))
        {
            $orders = Order::getUserOrders($userID);
            $event = [];		

            foreach ($orders as $key => $value)
            {
                $Event = new \yii2fullcalendar\models\Event();
                $Event->id = $value['key'];
                $Event->title = $value['title'];
                $Event->start = $value['start'];
                $events[] = $Event;
            }
        }
         \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
       // echo "!@#" . Json::encode($x) . "!@#";
        echo "!@#" . Json::encode($events) . "!@#";
        Yii::$app->end();
    }  
    
    public function actionNight_shift_approval() {
        //http://www.yiiframework.com/doc-2.0/yii-data-sqldataprovider.html
        $department = 0;
        $department = Yii::$app->db->createCommand('
            SELECT department
                FROM auth_assignment 
                JOIN user
                on user_id = user.id
                WHERE user_id = :managerID
                AND item_name = "מנהל מחלקה"    
            ', [':managerID' => Yii::$app->user->identity->id])->queryScalar();
        
        if($department > 0)
        {
            $count = Yii::$app->db->createCommand('
                SELECT count(1)
                    FROM `order`
                    JOIN user
                    ON userId = user.id
                    WHERE shift = 4
                    AND department = ' . $department
                , [':managerID' => Yii::$app->user->identity->id])->queryScalar();
            
            $dataProvider = new SqlDataProvider([
                'sql' => 'SELECT `order`.id, orderDate, orderApproved, userId, shift, firstName, lastName, department
                        FROM `order`
                        JOIN user
                        ON userId = user.id
                        WHERE shift = 4
                        AND department = ' . $department . " " .
                        'AND orderDate >= CURDATE()',
                'totalCount' => $count,
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]);

            // get the user records in the current page
            $models = $dataProvider->getModels();
        }

        //die($count);
               
        return $this->render('night_shift_approval', [
            'model' => $models,
            'dataProvider' => $dataProvider,
        ]);
    } 
    
    public function actionUpdate_night_shift($id, $approvedStatus) {
        $connection = Yii::$app->db;        
        $updateNightShift = $connection->createCommand('
            UPDATE
                `order`
                SET orderApproved = :approvedStatus
                WHERE id = :id
            ', [':id' => $id, ':approvedStatus' => $approvedStatus]);
        $updateNightShift->execute();        

        $this->redirect('night_shift_approval');
    }
    
    public function actionCan_update_order($id) {    
        $sql = "SELECT " .
            "orderDate, " .
            "shift " .
            "FROM `order` " .
            "WHERE id = " . $id;
        try {
                $connection = Yii::$app->db;
                $model = $connection->createCommand($sql);
                $rows = $model->queryOne();  

                if ($rows)
                {
                    $dayOfWeek = date('w', strtotime($rows['orderDate']));

                    switch ($dayOfWeek) 
                    {
                        case 0:
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                            $dow = 'w';
                            break;
                        case 5:
                            $dow = 'f';
                    } 
                    $sql = "SELECT " .
                        "shift, " .
                        "daydiff, " .
                        "mealHour " .
                        "FROM orderTableValidation " .
                        "WHERE dow = '" . $dow . "' " .
                        "AND shift = " . $rows['shift'];
                    try {
                        $connection = Yii::$app->db;
                        $model = $connection->createCommand($sql);
                        $validationRows = $model->queryOne();  
                        
                        if ($validationRows)
                        {
                            $maxOrderDate = strtotime ( $validationRows["daydiff"] . " day" , strtotime ($rows['orderDate']) );
                            $now = strtotime(date('Y-m-d H:i:s'));

                            $maxOrderDate = date('Y-m-d',  $maxOrderDate);
                            $maxOrderDate = $maxOrderDate . " " . $validationRows['mealHour'];
                            $maxOrderDate = strtotime($maxOrderDate);

                            if ($now - $maxOrderDate <= 0)
                                return "!@#" . 1 . "!@#";
                            else
                                return "!@#" . 0 . "!@#";
                        }
                    } catch (Exception $ex) {
                        print_r($e->getMessage());
                        return "!@#" . 0 . "!@#";
                    }
                }            
        } catch (Exception $ex) {
            print_r($e->getMessage());
            return "!@#" . 0 . "!@#";
        }
    }
}
