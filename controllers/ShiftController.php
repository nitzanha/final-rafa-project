<?php

namespace app\controllers;

use Yii;
use app\models\Shift;
use app\models\ShiftSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * ShiftController implements the CRUD actions for Shift model.
 */
class ShiftController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Shift models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ShiftSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Shift model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Shift model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Shift();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Shift model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Shift model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Shift model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Shift the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Shift::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    public function actionGet_shifts()
    {
        $shifts = shift::getShifts();
        $buttons = "";

        foreach ($shifts as $key => $value)
        {
            $buttons .= '<button class"btn" id="shiftID_' . $key . '" name="Order[shift]" value="' . $key . '">  <span>' . $value . '</span>  </button>';
            //$buttons .= '<button class"btn" id="shiftID" name="Order[shift]" value="' . $key . '">  <span>' . $value . '</span>  </button>';
        }      
        echo "!@#" . $buttons . "!@#";
    } 
    
    public function actionGet_relevant_shifts()
    {                
        $shifts = shift::getShifts();
        
        if (Yii::$app->request->get()) 
        {
            $datesArray = explode(",", $_GET['targetDates']);
            for ($x = 0; $x < count($datesArray); $x++) {
                
                $dayOfWeek = date('w', strtotime($datesArray[$x]));

                switch ($dayOfWeek) 
                {
                    case 0:
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                        $dow = 'w';
                        break;
                    case 5:
                        $dow = 'f';
                }                
                $buttons = "";
                foreach ($shifts as $key => $value)
                {
                    $sql = "SELECT " .
                        "shift, " .
                        "daydiff, " .
                        "mealHour " .
                        "FROM orderTableValidation " .
                        "WHERE dow = '" . $dow . "' " .
                        "AND shift = " . $key;
                    try
                    {
                        $connection = Yii::$app->db;
                        $model = $connection->createCommand($sql);
                        $rows = $model->queryOne();  
                        
                        if ($rows)
                        {
                            $maxOrderDate = strtotime ( $rows["daydiff"] . " day" , strtotime ($datesArray[$x]) );
                            $now = strtotime(date('Y-m-d H:i:s'));

                            $maxOrderDate = date('Y-m-d',  $maxOrderDate);
                            $maxOrderDate = $maxOrderDate . " " . $rows['mealHour'];
                            $maxOrderDate = strtotime($maxOrderDate);

                            if ($now - $maxOrderDate <= 0)
                            {
                                // check if employee have already order to same shift in same date
                                if ($_GET['employeeID'] == "")
                                    $userID = Yii::$app->user->getId();
                                else
                                    $userID = $_GET['employeeID'];
                                $sql = 'SELECT * FROM `order` WHERE userId = ' . $userID . ' AND orderDate = "' . $datesArray[$x] . '" AND shift = ' . $key;
                                try
                                {
                                    $orderConnection = Yii::$app->db;
                                    $orderModel = $orderConnection->createCommand($sql);
                                    $orderRows = $orderModel->queryOne();
                                    /*
                                    if (empty($rows))
                                       return "!@#" . 0 . "!@#";
                                    else
                                        return "!@#" . 1 . "!@#";
                                    */
                                } 
                                catch (\Exception $e) 
                                {
                                    //file_put_contents("/var/www/html/testYair.log", "error " .  print_r($e->getMessage(), true) . "\n" , FILE_APPEND);
                                    print_r($e->getMessage());
                                    //return "!@#" . 0 . "!@#";
                                }   
                                if (empty($orderRows))
                                    $buttons .= '<button class"btn" id="shiftID_' . $key . '" name="Order[shift]" value="' . $key . '">  <span>' . $value . 
                                        '</span>  </button>';
                                $orderModel = null;
                                unset($orderModel);
                            }
                            /*
                            else 
                            {
                              //  file_put_contents("/var/www/html/testYair.log", "לא ניתן להזמין ארוחת " . $value . " לתאריך " . $datesArray[$x] .  "\n" , FILE_APPEND);
                            }
                             * 
                             */
                        }
                    }
                    catch (\Exception $e) 
                    {
                        //file_put_contents("/var/www/html/testYair.log", "error " .  print_r($e->getMessage(), true) . "\n" , FILE_APPEND);
                        print_r($e->getMessage());
                        return "!@#" . 0 . "!@#";
                    }                     
                }
                echo "!@#" . $buttons . "!@#";
            }   
        }
    }  
    
    /*
    public static function getVendors()
    {
        $allVendors = self::find()->all();
        $allVendorsArray = ArrayHelper::
                                map($allVendors, 'id', 'name');
        return $allVendorsArray;						
    } 
     * 
     */   
}
