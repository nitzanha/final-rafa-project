<?php

namespace app\controllers;

use Yii;
use app\models\Meal;
use app\models\MealSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
 * MealController implements the CRUD actions for Meal model.
 */
class MealController extends Controller {

    public function beforeAction($action) {
        if (Yii::$app->language == "he") {
            ?>
            <link href="/rafafoodproject/web/assets/8b0376e8/css/bootstrap.css" rel="stylesheet">
            <link href="/rafafoodproject/web/assets/8b0376e8/css/bootstrap-rtl.css" rel="stylesheet">
            <?php
        } else {
            ?>
            <link href="/rafafoodproject/web/assets/8b0376e8/css/bootstrap.css" rel="stylesheet">
            <?php
        }

        return parent::beforeAction($action);
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                /*
                'actions' => [
                    'delete' => ['POST'],
                ],
                 * 
                 */
            ],
        ];
    }

    /**
     * Lists all Meal models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new MealSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Meal model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Meal model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Meal();
        if ($model->load(Yii::$app->request->post())) 
        {
            $image = UploadedFile::getInstance($model, 'picture');
            if (!is_null($image)) {
                $model->image_src_filename = $image->name;
                $ext = end((explode(".", $image->name)));
                // generate a unique file name to prevent duplicate filenames
                $model->image_web_filename = Yii::$app->security->generateRandomString() . ".{$ext}";
                // the path to save file, you can set an uploadPath
                // in Yii::$app->params (as used in example below)                       
                Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/web/uploads/images/';
                $path = Yii::$app->params['uploadPath'] . $model->image_web_filename;
                $image->saveAs($path);
            }
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                var_dump($model->getErrors());
                die();
            }
        }
        return $this->render('create', [
                'model' => $model,
        ]);
    }

    /**
     * Updates an existing Meal model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        
        if ($model->load(Yii::$app->request->post())) 
        {
            $image = UploadedFile::getInstance($model, 'picture');
            if (!is_null($image)) {
                $model->image_src_filename = $image->name;
                $ext = end((explode(".", $image->name)));
                // generate a unique file name to prevent duplicate filenames
                $model->image_web_filename = Yii::$app->security->generateRandomString() . ".{$ext}";
                // the path to save file, you can set an uploadPath
                // in Yii::$app->params (as used in example below)                       
                Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/web/uploads/images/';
                $path = Yii::$app->params['uploadPath'] . $model->image_web_filename;
                $image->saveAs($path);
            }
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                var_dump($model->getErrors());
                die();
            }
        }
        return $this->render('create', [
                'model' => $model,
        ]);        
/*
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                    'model' => $model,
            ]);
        }
 * 
 */
    }

    /**
     * Deletes an existing Meal model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $meal = Meal::findOne($id);
        if (!empty($meal['image_web_filename']))
            if (unlink(Yii::$app->basePath . '/web/uploads/images/' . $meal['image_web_filename']))
            {
        
               // file_put_contents("/var/www/html/testYair.log", "meal: " . print_r($meal, true) . "\n", FILE_APPEND);
        
                $this->findModel($id)->delete();
            }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Meal model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Meal the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Meal::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionGet_meals_by_type_id() {
        if (Yii::$app->request->isAjax) {
            if (Yii::$app->request->post()) {
                $data = Yii::$app->request->post();
                $mealCategoryID = $data['mealType'];
            } else {
                $data = Yii::$app->request->get();
                $mealCategoryID = $data['mealType'];
            }

            $allCategories = Meal::find()
                ->where(['categoryId' => $mealCategoryID])
                ->all();

            $allCategoriesArray = ArrayHelper::
                map($allCategories, 'id', 'name');

            $select = "";
            foreach ($allCategoriesArray as $key => $value) {
                // $select .= '<label><input type="radio" id="mealsID" name="Order[mealId]" value="' . $key . '">  <span>' . $value . '</span>  </label>';
                //$select .= '<button type="button" id="mealsID_' . $key . '" name="Order[mealId]" value="' . $key . '">' . $value;
                $select .= '<tr>' .
                    '<td>' . $key . '</td>' .
                    '<td>' . $value . '</tr>' .
                    '</tr>';
            }
        }
        return "!@#" . $select . "!@#";
    }

    public function actionGet_meals_by_parameters() {
        if (Yii::$app->request->isAjax) {
            if (Yii::$app->request->post()) {
                $data = Yii::$app->request->post();
                // file_put_contents("/var/www/html/testYair.log", "post in get_meals_by_parameters: " . print_r(Yii::$app->request->post(), true) . "\n", FILE_APPEND);
            } else {
                $data = Yii::$app->request->get();
                //file_put_contents("/var/www/html/testYair.log", "post in get_meals_by_parameters: " . print_r(Yii::$app->request->get(), true) . "\n", FILE_APPEND);
            }

            $mealCategoryID = $data['mealType'];
            $shift = $data['shift'];
            $categoryType = $data['categoryType'];

            $mealSql = (new \yii\db\Query())
                ->select(['id', 'name', 'mealExtras'])
                ->from('meal')
                ->where(['categoryId' => $mealCategoryID, 'mealCategoryType' => $categoryType, 'mealShift' => $shift])
                ->createCommand();

            $mealRows = $mealSql->queryAll();
            
            //file_put_contents("/var/www/html/testYair.log", "mealExtras: " . print_r($mealRows[0]['mealExtras'], true) . "\n", FILE_APPEND);
            if (!empty($mealRows[0]['mealExtras']))
                $extras = explode(",", $mealRows[0]['mealExtras']);
            
            //file_put_contents("/var/www/html/testYair.log", "extras: " . print_r($extras, true) . "\n", FILE_APPEND);
            //    file_put_contents("/var/www/html/testYair.log", "mealRows in get_meals_by_parameters: " . print_r($mealRows, true) . "\n", FILE_APPEND);
            /*
              $allCategories = Meal::find()
              ->where(['categoryId' => $mealCategoryID])
              ->all();
             */
            
            //file_put_contents("/var/www/html/testYair.log", "mealRows: " . print_r($mealRows, true) . "\n", FILE_APPEND);             
            /*
            $allCategoriesArray = ArrayHelper::
                map($mealRows, 'id', 'name');

            file_put_contents("/var/www/html/testYair.log", "allCategoriesArray: " . print_r($allCategoriesArray, true) . "\n", FILE_APPEND);            
            */
            if (!empty($extras))
            {
                $mealExtras = '<select id="mealExtras" multiple="multiple">';
                foreach($extras as $item) {
                    $mealExtras .= '<option value="' . trim($item) . '">' . $item . '</option>';
                    //$mealExtras .= '<option>' . $item . '</option>';
                }
                $mealExtras .= '</select>';
            }
            else 
            {
                $mealExtras = "";
            }
            
            $select = "";
            //foreach ($allCategoriesArray as $key => $value) {
            foreach ($mealRows as $key => $value) {    
               // file_put_contents("/var/www/html/testYair.log", "key: " . $key . " value: " . print_r($value, true) . "\n", FILE_APPEND);
                $select .= '<tr>' .
                    '<td>' . $value['id'] . '</td>' .
                    '<td>' . $value['name'] . '</td>' .
                    //'<td>' . $value['mealExtras'] . '</td>' .
                    '<td>' . $mealExtras . '</td>' .
                    '</tr>';
            }
        }
        return "!@#" . $select . "!@#";
    }   
}
