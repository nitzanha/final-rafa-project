<?php
namespace app\commands;

use Yii;
use yii\console\Controller;


class AddruleController extends Controller
{

	public function actionOwnlead()
	{	
		$auth = Yii::$app->authManager;	
		$rule = new \app\rbac\OwnLeadRule;
		$auth->add($rule);
	}
	
	public function actionOwnuser()
	{	
		$auth = Yii::$app->authManager;	
		$rule = new \app\rbac\OwnUserRule;
		$auth->add($rule);
	}
}