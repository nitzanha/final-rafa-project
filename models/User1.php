<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user1".
 *
 * @property integer $id
 * @property string $userId
 * @property string $password
 * @property string $authKey
 * @property string $firstName
 * @property string $lastName
 * @property integer $department
 * @property integer $userType
 * @property string $email
 * @property string $endDate
 * @property integer $status
 * @property string $comment
 */
class User1 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user1';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userId', 'password', 'authKey', 'firstName', 'department', 'userType'], 'required'],
            [['department', 'userType', 'status'], 'integer'],
            [['endDate'], 'safe'],
            [['userId', 'password', 'authKey', 'firstName', 'lastName', 'email', 'comment'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'userId' => 'User ID',
            'password' => 'Password',
            'authKey' => 'Auth Key',
            'firstName' => 'First Name',
            'lastName' => 'Last Name',
            'department' => 'Department',
            'userType' => 'User Type',
            'email' => 'Email',
            'endDate' => 'End Date',
            'status' => 'Status',
            'comment' => 'Comment',
        ];
    }
}
