<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Meal;

/**
 * MealSearch represents the model behind the search form about `app\models\Meal`.
 */
class MealSearch extends Meal
{
    public $vendorName;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'vendorId', 'categoryId', 'status'], 'integer'],
            [['name', 'details', 'picture', 'comment', 'mealExtras'], 'safe'],
            [['vendorName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Meal::find();

        $query->joinWith(['vendor']);
        
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        
        $dataProvider->sort->attributes['vendorName'] = [
            'asc' => ['vendor.vendorName' => SORT_ASC],
            'desc' => ['vendor.vendorName' => SORT_DESC],
        ];        

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        
        
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'vendorId' => $this->vendorId,
            'categoryId' => $this->categoryId,
            'status' => $this->status,
            //'mealExtras' => $this->mealExtras,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'details', $this->details])
            ->andFilterWhere(['like', 'picture', $this->picture])
            ->andFilterWhere(['like', 'comment', $this->comment])
            ->andFilterWhere(['like', 'mealExtras', $this->mealExtras])
            ->andFilterWhere(['like', 'vendor.vendorName', $this->vendorName]);

        return $dataProvider;
    }
}
