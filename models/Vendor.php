<?php

namespace app\models;

use yii\helpers\ArrayHelper;
use Yii;

/**
 * This is the model class for table "vendor".
 *
 * @property integer $id
 * @property string $vendorName
 * @property string $email
 * @property string $fax
 * @property string $phone
 * @property string $address
 * @property string $startTime
 * @property string $endTime
 * @property integer $status
 * @property string $comment
 */
class Vendor extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'vendor';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['vendorName'], 'required'],
            [['startTime', 'endTime'], 'safe'],
            [['status'], 'integer'],
            [['vendorName', 'email', 'fax', 'phone', 'address', 'comment'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => yii::t('yii', 'ID'),
            'vendorName' => yii::t('yii', 'name'),
            'email' => yii::t('yii', 'email'),
            'fax' => yii::t('yii', 'fax'),
            'phone' => yii::t('yii', 'phone'),
            'address' => yii::t('yii', 'address'),
            'startTime' => yii::t('yii', 'startTime'),
            'endTime' => yii::t('yii', 'endTime'),
            'status' => yii::t('yii', 'status'),
            'comment' => yii::t('yii', 'comment'),
        ];
    }

    public function getOrder() {
        return $this->hasMany(Order::className(), ['id' => 'vendorId'])->viaTable('meal', ['vendorId' => 'id']);
    }

    public function getStatusItem() {
        return $this->hasOne(Status::className(), ['id' => 'status']);
    }

    public static function getVendors() {
        $allVendors = self::find()->all();
        $allVendorsArray = ArrayHelper::
            map($allVendors, 'id', 'vendorName');
        return $allVendorsArray;
    }

    public static function getVendorsWithAllVendors() {
        $allVendors = self::getVendors();
        $allVendors[null] = 'All Vendors';
        $allVendors = array_reverse($allVendors, true);
        return $allVendors;
    }

}
