<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Order;
//use app\models\User;
//use app\models\Shift;

/**
 * OrderSearch represents the model behind the search form about `app\models\Order`.
 */
class OrderSearch extends Order {

    public $firstName;  //user first name
    public $name;   //meal name
    public $categoryTypeName;
    public $categoryName;
    public $shiftName;
    public $vendorName;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'userId', 'mealId', 'shift', 'categoryTypeId', 'categoryId', 'status'], 'integer'],
            [['orderDate', 'lastUpdate', 'comment'], 'safe'],
            [['firstName', 'name','categoryTypeName', 'categoryName', 'shiftName', 'vendorName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        //$query = Order::find();
        $query = Order::find()->innerJoinWith('vendor', true);
       //$query->innerJoinWith('shift', true);
        $query->joinWith(['user', 'categoryType', 'category', 'shift']);
        
        //$query->joinWith(['user', 'meal', 'categoryType', 'category', 'shift']);
        //$query = Order::find()->innerJoinWith('vendor', true);
        //$query = Order::find()->innerJoinWith('shift', true);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['firstName'] = [
            'asc' => ['user.firstName' => SORT_ASC],
            'desc' => ['user.firstName' => SORT_DESC],
        ];  
        $dataProvider->sort->attributes['name'] = [
            'asc' => ['meal.name' => SORT_ASC],
            'desc' => ['meal.name' => SORT_DESC],
        ];     
        
        $dataProvider->sort->attributes['categoryTypeName'] = [
            'asc' => ['categorytype.categoryTypeName' => SORT_ASC],
            'desc' => ['categorytype.categoryTypeName' => SORT_DESC],
        ];        
          
        $dataProvider->sort->attributes['categoryName'] = [
            'asc' => ['category.categoryName' => SORT_ASC],
            'desc' => ['category.categoryName' => SORT_DESC],
        ]; 
        
        $dataProvider->sort->attributes['shiftName'] = [
            'asc' => ['shift.shiftName' => SORT_ASC],
            'desc' => ['shift.shiftName' => SORT_DESC],
        ];   
        
        $dataProvider->sort->attributes['vendorName'] = [
            'asc' => ['vendor.vendorName' => SORT_ASC],
            'desc' => ['vendor.vendorName' => SORT_DESC],
        ];         
        
/*
          $this->load($params);

          if (!$this->validate()) {
          // uncomment the following line if you do not want to return any records when validation fails
           $query->where('0=1');
          return $dataProvider;
          }
*/         

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }
        
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            //'userId' => $this->userId,
            //'mealId' => $this->mealId,
            //'orderDate' => $this->orderDate,
            'lastUpdate' => $this->lastUpdate,
            //'shift' => $this->shift,
            //'categoryTypeId' => $this->categoryTypeId,
            //'categoryId' => $this->categoryId,
            'status' => $this->status,
        ]);   
        
        $query->andFilterWhere(['like', 'user.firstName', $this->firstName]);
        $query->andFilterWhere(['like', 'meal.name', $this->name]);
        $query->andFilterWhere(['like', 'categorytype.categoryTypeName', $this->categoryTypeName]);
        $query->andFilterWhere(['like', 'category.categoryName', $this->categoryName]);
        $query->andFilterWhere(['like', 'shift.shiftName', $this->shiftName]);
        $query->andFilterWhere(['like', 'vendor.vendorName', $this->vendorName]);
        $query->andFilterWhere(['like', 'comment', $this->comment]);
        if(isset($this->orderDate) && $this->orderDate!=''){
            $date_explode = explode(" - ", $this->orderDate);
            $date1 = trim($date_explode[0]);
            $date2= trim($date_explode[1]);
            $query->andFilterWhere(['between', 'orderDate', $date1, $date2]);
        }        

/*
 * http://www.yiiframework.com/wiki/621/filter-sort-by-calculated-related-fields-in-gridview-yii-2-0/
 * http://www.yiiframework.com/wiki/653/displaying-sorting-and-filtering-model-relations-on-a-gridview/
 * https://amnah.net/2013/12/22/how-to-display-sort-and-filter-related-model-data-in-gridview/
 * http://www.yiiframework.com/wiki/851/yii2-gridview-sorting-and-searching-with-a-junction-table-column-many-to-many-relationship/
 * yii2 gridview filter relation
 */           
        
        return $dataProvider;
    }    
    
}
