<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use Yii;

/**
 * This is the model class for table "meal".
 *
 * @property integer $id
 * @property string $name
 * @property string $details
 * @property integer $vendorId
 * @property integer $categoryId
 * @property string $picture
 * @property integer $status
 * @property string $comment
 */
class Meal extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'meal';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['name'], 'required'],
            [['vendorId', 'categoryId', 'status'], 'integer'],
            [['name', 'details', 'picture', 'comment'], 'string', 'max' => 255],
            [['picture'], 'safe'],
            [['picture'], 'file', 'extensions' => 'jpg, gif, png'],
            [['picture'], 'file', 'maxSize' => '1000000'],
            [['image_src_filename', 'image_web_filename'], 'string', 'max' => 255],
            [['mealExtras'], 'string', 'max' => 777],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'details' => 'Details',
            'vendorId' => 'Vendor ID',
            'categoryId' => 'Category ID',
            'picture' => 'Picture',
            'status' => 'Status',
            'comment' => 'Comment',
            'image_src_filename' => Yii::t('app', 'Filename'),
            'image_web_filename' => Yii::t('app', 'Pathname'),
            'mealExtras' => Yii::t('app', 'Extras'),
        ];
    }
/*
    public function getMeal() {
        return $this->hasOne(User::className(), ['Id' => 'id']);
    }
*/
    public function getCategoryItem() {
        return $this->hasOne(Category::className(), ['id' => 'categoryId']);
    }

    public function getStatusItem() {
        return $this->hasOne(Status::className(), ['id' => 'status']);
    }
    
    public function getVendor() {
        return $this->hasOne(Vendor::className(), ['id' => 'vendorId']);
    }    

    public function getVendorItem() {
        return $this->hasOne(Vendor::className(), ['id' => 'vendorId']);
    }

}
