<?php

namespace app\models;

use yii\helpers\ArrayHelper;
use Yii;

/**
 * This is the model class for table "usertype".
 *
 * @property integer $id
 * @property string $userTypeName
 * @property integer $status
 */
class Usertype extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'usertype';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['userTypeName'], 'required'],
            [['status'], 'integer'],
            [['userTypeName'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'userTypeName' => 'Name',
            'status' => 'Status',
        ];
    }

    public static function getUserTypes() {
        $allUserTypes = self::find()->all();
        $allUserTypesArray = ArrayHelper::
            map($allUserTypes, 'id', 'userTypeName');
        return $allUserTypesArray;
    }

    public static function getUserTypesWithAllUserTypes() {
        $allUserTypes = self::getUserTypes();
        $allUserTypes[null] = 'All UserTypes';
        $allUserTypes = array_reverse($allUserTypes, true);
        return $allUserTypes;
    }

}
