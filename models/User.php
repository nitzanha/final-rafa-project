<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use Yii;

class User extends ActiveRecord implements \yii\web\IdentityInterface {

    public $role;

    public static function tableName() {
        return 'user';
    }

    public function rules() {

        return [
            [['username', 'password', 'firstName', 'department', 'userType'], 'required'],
            [['department', 'userType', 'status'], 'integer'],
            [['endDate'], 'safe'],
            [['username'], 'unique'],
            [['username', 'password', 'authKey', 'firstName', 'lastName', 'email', 'comment'], 'string', 'max' => 255],
            ['role', 'safe'],
        ];
    }

    /**
      return self::findOne($id);
      }


      /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null) {
        throw new NotSupportedException('Not supported');

        return null;
    }

    public static function findIdentity($id) {
        return self::findOne($id);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByusername($username) {
        return self::findOne(['username' => $username]);
    }

    /**
     * @inheritdoc
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey() {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey) {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    //test hashed password

    public function validatePassword($password) {
        return $this->isCorrectHash($password, $this->password);
        ;
    }

    private function isCorrectHash($plaintext, $hash) {
        return Yii::$app->security->validatePassword($plaintext, $hash);
    }

    //hash password before saving
    public function beforeSave($insert) {
        $return = parent::beforeSave($insert);

        if ($this->isAttributeChanged('password'))
            $this->password = Yii::$app->security->
                generatePasswordHash($this->password);

        if ($this->isNewRecord)
            $this->authKey = Yii::$app->security->generateRandomString(32);

        return $return;
    }

    public function afterSave($insert, $changedAttributes) {
        $return = parent::afterSave($insert, $changedAttributes);

        $auth = Yii::$app->authManager;
        $roleName = $this->role;
        $role = $auth->getRole($roleName);
        if (\Yii::$app->authManager->getRolesByUser($this->id) == null) {
            $auth->assign($role, $this->id);
        } else {
            $db = \Yii::$app->db;
            $db->createCommand()->delete('auth_assignment', ['user_id' => $this->id])->execute();
            $auth->assign($role, $this->id);
        }

        return $return;
    }

    //create fullname pseuodo field
    public function getFullname() {
        return $this->firstname . ' ' . $this->lastname;
    }

    //A method to get an array of all users models/User
    public static function getUsers() {
        $users = ArrayHelper::
            map(self::find()->all(), 'id', 'firstName');
        return $users;
    }
    
    public static function getEmployees() {
        $users = ArrayHelper::
            map(self::find()->where(['<>', 'firstName', 'admin'])->all(), 'id', 'firstName');
        return $users;
    }    

    public static function getRoles() {

        $rolesObjects = Yii::$app->authManager->getRoles();
        $roles = [];
        foreach ($rolesObjects as $id => $rolObj) {
            $roles[$id] = $rolObj->name;
        }

        return $roles;
    }

    public function attributeLabels() {
        return [

            'id' => yii::t('yii', 'ID'),
            'name' => yii::t('yii', 'name'),
            'email' => yii::t('yii', 'email'),
            'startTime' => yii::t('yii', 'startTime'),
            'endDate' => yii::t('yii', 'endDate'),
            'status' => yii::t('yii', 'status'),
            'comment' => yii::t('yii', 'comment'),
            'username' => yii::t('yii', 'username'),
            'password' => yii::t('yii', 'password'),
            'firstName' => yii::t('yii', 'firstName'),
            'lastName' => yii::t('yii', 'lastName'),
            'department' => yii::t('yii', 'department'),
            'userType' => yii::t('yii', 'userType'),
        ];
    }

    public function getStatusItem() {
        return $this->hasOne(Status::className(), ['id' => 'status']);
    }

    public function getUserTypeItem() {
        return $this->hasOne(UserType::className(), ['id' => 'userType']);
    }

    public function getDepartmentItem() {
        return $this->hasOne(Department::className(), ['depId' => 'department']);
    }

}
