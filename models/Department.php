<?php

namespace app\models;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * This is the model class for table "department".
 *
 * @property integer $id
 * @property integer $depId
 * @property string $depName
 * @property string $endDate
 * @property string $location
 * @property integer $status
 * @property string $comment
 */
class Department extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'department';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['depId', 'status'], 'integer'],
            [['endDate'], 'safe'],
            [['depName', 'location', 'comment'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'depId' => 'Dep ID',
            'depName' => 'Name',
            'endDate' => 'End Date',
            'location' => 'Location',
            'status' => 'Status',
            'comment' => 'Comment',
        ];
    }
	
	public static function getDepartment()
	{
		$allDepartment = self::find()->all();
		$allDepartmentArray = ArrayHelper::
					map($allDepartment, 'depId', 'depName');
		return $allDepartmentArray;						
	}
	
	public static function getDepartmentWithAllDepartment()
	{
		$allDepartment = self::getDepartment();
		$allDepartment[null] = 'All Department';
		$allDepartment = array_reverse ( $allDepartment, true );
		return $allDepartment;	
	}
	
	
}
