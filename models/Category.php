<?php

namespace app\models;

use yii\helpers\ArrayHelper;
use Yii;

/**
 * This is the model class for table "category".
 *
 * @property integer $id
 * @property string $categoryName
 * @property integer $categoryType
 * @property integer $status
 * @property string $comment
 */
class Category extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['categoryName'], 'required'],
            [['categoryType', 'status'], 'integer'],
            [['categoryName', 'comment'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'categoryName' => 'Name',
            'categoryType' => 'Category Type',
            'status' => 'Status',
            'comment' => 'Comment',
        ];
    }

    public static function getCategories() {
        $allCategories = self::find()->all();
        $allCategoriesArray = ArrayHelper::
            map($allCategories, 'id', 'categoryName');
        return $allCategoriesArray;
    }

    public static function getCategoriesWithAllCategories() {
        $allCategories = self::getCategories();
        $allCategories[null] = 'All Categories';
        $allCategories = array_reverse($allCategories, true);
        return $allCategories;
    }

    public function getCategorytypesItem() {
        return $this->hasOne(Categorytype::className(), ['id' => 'categoryName']);
    }

}
