<?php

namespace app\models;
use Yii;
use yii\helpers\ArrayHelper;
use app\models\Shift;


/**
 * This is the model class for table "shift".
 *
 * @property integer $id
 * @property string $shiftName
 * @property integer $status
 * @property string $comment
 */
class Shift extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'shift';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['shiftName', 'shiftId'], 'required'],
            [['status', 'shiftId'], 'integer'],
            [['shiftName', 'comment'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'shiftId' => 'shift ID',
            'shiftName' => 'Name',
            'status' => 'Status',
            'comment' => 'Comment',
        ];
    }

    public static function getShifts() {
        $allShifts = self::find()->all();
        $allShiftsArray = ArrayHelper::
            map($allShifts, 'shiftId', 'shiftName');
        return $allShiftsArray;
    }

    public static function getShiftsWithAllShifts() {
        $allShifts = self::getShifts();
        $allShifts[null] = 'All Shifts';
        $allShifts = array_reverse($allShifts, true);
        return $allShifts;
    }    
    
    public static function getShiftsForUpdate()
    {   
        $sql = "SELECT " .
            "orderDate, " .
            "shift " .
            "FROM `order` " .
            "WHERE id = " . Yii::$app->getRequest()->getQueryParam('id');
        try {
                $connection = Yii::$app->db;
                $model = $connection->createCommand($sql);
                $rows = $model->queryOne();  

                if ($rows)
                {
                    $dayOfWeek = date('w', strtotime($rows['orderDate']));

                    switch ($dayOfWeek) 
                    {
                        case 0:
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                            $dow = 'w';
                            break;
                        case 5:
                            $dow = 'f';
                    } 
                    $sql = "SELECT " .
                        "shift, " .
                        "daydiff, " .
                        "mealHour " .
                        "FROM orderTableValidation " .
                        "WHERE dow = '" . $dow . "' " .
                        "AND shift = " . $rows['shift'];
                    try {
                        $connection = Yii::$app->db;
                        $model = $connection->createCommand($sql);
                        $validationRows = $model->queryOne();  
                        
                        if ($validationRows)
                        {
                            $maxOrderDate = strtotime ( $validationRows["daydiff"] . " day" , strtotime ($rows['orderDate']) );
                            $now = strtotime(date('Y-m-d H:i:s'));

                            $maxOrderDate = date('Y-m-d',  $maxOrderDate);
                            $maxOrderDate = $maxOrderDate . " " . $validationRows['mealHour'];
                            $maxOrderDate = strtotime($maxOrderDate);

                            if ($now - $maxOrderDate <= 0)
                            {
                                $shiftSQL = "SELECT " .
                                    "shiftName " .
                                    "FROM shift " .
                                    "WHERE id = " . $rows['shift'];
                                $connection = Yii::$app->db;
                                $model = $connection->createCommand($shiftSQL);
                                $shiftRow = $model->queryOne();
                                
                                $buttons = '<button class"btn" id="shiftID_' . $rows['shift'] . '" name="Order[shift]" value="' . $rows['shift'] . '">  <span>' . 
                                    $shiftRow['shiftName'] . '</span>  </button>';
                                $buttons = [$rows['shift'] => $shiftRow['shiftName']];
                                file_put_contents("/var/www/html/testYair.log", "buttons " .  print_r($buttons, true) . "\n" , FILE_APPEND);
                                return $buttons;
                            }
                        }
                    } catch (Exception $ex) {
                        print_r($e->getMessage());
                        echo 0;
                    }
                }            
        } catch (Exception $ex) {
            print_r($e->getMessage());
            return 0;
        }
    }            
}
