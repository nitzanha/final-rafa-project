<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "uploads".
 *
 * @property integer $uploadID
 * @property string $filename
 * @property string $file_src_filename
 * @property string $file_web_filename
 */
class Uploads extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'uploads';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['filename', 'file_src_filename', 'file_web_filename'], 'string', 'max' => 255],
            [['filename'], 'safe'],
            [['filename'], 'file', 'extensions' => 'xls'],
            [['filename'], 'file', 'maxSize' => '1000000'],            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'uploadID' => 'Upload ID',
            'filename' => 'Filename',
            'file_src_filename' => 'File Src Filename',
            'file_web_filename' => 'File Web Filename',
        ];
    }
}
