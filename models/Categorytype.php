<?php

namespace app\models;

use yii\helpers\ArrayHelper;
use Yii;

/**
 * This is the model class for table "categorytype".
 *
 * @property integer $id
 * @property string $name
 */
class Categorytype extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'categorytype';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['categoryTypeName'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'categoryTypeName' => 'Name',
        ];
    }

    public static function getCategorytypesByShift() {
        //if ($_GET)
            //file_put_contents("/var/www/html/testYair.log", "get in getCategorytypes: " . print_r($_GET, true) . "\n", FILE_APPEND);
        
       // if ($_POST)
            //file_put_contents("/var/www/html/testYair.log", "post in getCategorytypes: " . print_r($_POST, true) . "\n", FILE_APPEND);        

        $shift = Yii::$app->request->get('shift');

        //file_put_contents("/var/www/html/testYair.log", "shift in getCategorytypes: " . print_r($shift, true) . "\n", FILE_APPEND);

        $mealSql = (new \yii\db\Query())
            ->select(['categorytype.id', 'categorytype.categoryTypeName'])
            ->distinct()
            ->from('meal')
            ->where(['mealShift' => $shift])
            ->join('INNER JOIN', 'categorytype', 'categorytype.id = mealCategoryType')
            ->createCommand();

        //file_put_contents("/var/www/html/testYair.log", "sql: " . print_r($mealSql->sql, true) . "\n", FILE_APPEND);
        //file_put_contents("/var/www/html/testYair.log", "params: " . print_r($mealSql->params, true) . "\n", FILE_APPEND);

        $mealRows = $mealSql->queryAll();
        //file_put_contents("/var/www/html/testYair.log", "mealRows: " . print_r($mealRows, true) . "\n", FILE_APPEND);

        //$allCategorytypes = self::find()->all();
        $allCategorytypesArray = ArrayHelper::
            map($mealRows, 'id', 'categoryTypeName');
        return $allCategorytypesArray;
    }
    
	public static function getCategorytypes()
	{
		$allCategorytypes = self::find()->all();
		$allCategorytypesArray = ArrayHelper::
					map($allCategorytypes, 'id', 'categoryTypeName');
		return $allCategorytypesArray;						
	}    

    public static function getCategorytypesWithAllCategorytypes() {
        $allCategorytypes = self::getCategorytypes();
        $allCategorytypes[null] = 'All Category types';
        $allCategorytypes = array_reverse($allCategorytypes, true);
        return $allCategorytypes;
    }

    public static function getCategorytypesByOrder() {
        $sql = "SELECT " .
            "orderDate, " .
            "shift " .
            "FROM `order` " .
            "WHERE id = " . Yii::$app->getRequest()->getQueryParam('id');
        try {
                $connection = Yii::$app->db;
                $model = $connection->createCommand($sql);
                $rows = $model->queryOne();  

                if ($rows)
                {
                    $dayOfWeek = date('w', strtotime($rows['orderDate']));

                    switch ($dayOfWeek) 
                    {
                        case 0:
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                            $dow = 'w';
                            break;
                        case 5:
                            $dow = 'f';
                    } 
                    $sql = "SELECT " .
                        "shift, " .
                        "daydiff, " .
                        "mealHour " .
                        "FROM orderTableValidation " .
                        "WHERE dow = '" . $dow . "' ";
                    try {
                        $connection = Yii::$app->db;
                        $model = $connection->createCommand($sql);
                        $validationRows = $model->queryAll(); 
                        $buttons = "";
                        foreach ($validationRows as $key => $value) {    
                            //if ($validationRows)
                            //{
                                $maxOrderDate = strtotime ( $value["daydiff"] . " day" , strtotime ($rows['orderDate']) );
                                $now = strtotime(date('Y-m-d H:i:s'));

                                $maxOrderDate = date('Y-m-d',  $maxOrderDate);
                                $maxOrderDate = $maxOrderDate . " " . $value['mealHour'];
                                $maxOrderDate = strtotime($maxOrderDate);

                                if ($now - $maxOrderDate <= 0)
                                {
                                    /*
                                    $shiftSQL = "SELECT " .
                                        "shiftName " .
                                        "FROM shift " .
                                        "WHERE id = " . $rows['shift'];
                                    $connection = Yii::$app->db;
                                    $model = $connection->createCommand($shiftSQL);
                                    $shiftRow = $model->queryOne();
                                    */
                                    $mealSql = (new \yii\db\Query())
                                        ->select(['categorytype.id', 'categorytype.categoryTypeName'])
                                        ->distinct()
                                        ->from('meal')
                                        ->where(['mealShift' => $rows['shift']])
                                        ->join('INNER JOIN', 'categorytype', 'categorytype.id = mealCategoryType')
                                        ->createCommand();
                                    $mealRows = $mealSql->queryAll();                    
                                    $allCategorytypesArray = ArrayHelper::
                                        map($mealRows, 'id', 'categoryTypeName');
                                    return $allCategorytypesArray;
                    /*
                                    $buttons .= '<button class"btn" id="shiftID_' . $rows['shift'] . '" name="Order[shift]" value="' . $rows['shift'] . '">  <span>' . 
                                        $shiftRow['shiftName'] . '</span>  </button>';
                                    $buttons = [$rows['shift'] => $shiftRow['shiftName']];
                                    file_put_contents("/var/www/html/testYair.log", "buttons " .  print_r($buttons, true) . "\n" , FILE_APPEND);
                                    return $buttons;
                     * 
                     */
                                }
                            //}
                        }
                    } catch (Exception $ex) {
                        print_r($e->getMessage());
                        echo 0;
                    }                    
                    /*
                    //file_put_contents("/var/www/html/testYair.log", "shift in getCategorytypes: " . print_r($shift, true) . "\n", FILE_APPEND);

                    $mealSql = (new \yii\db\Query())
                        ->select(['categorytype.id', 'categorytype.categoryTypeName'])
                        ->distinct()
                        ->from('meal')
                        ->where(['mealShift' => $rows['shift']])
                        ->join('INNER JOIN', 'categorytype', 'categorytype.id = mealCategoryType')
                        ->createCommand();

                    //file_put_contents("/var/www/html/testYair.log", "sql: " . print_r($mealSql->sql, true) . "\n", FILE_APPEND);
                    //file_put_contents("/var/www/html/testYair.log", "params: " . print_r($mealSql->params, true) . "\n", FILE_APPEND);

                    $mealRows = $mealSql->queryAll();
                    //file_put_contents("/var/www/html/testYair.log", "mealRows: " . print_r($mealRows, true) . "\n", FILE_APPEND);

                    //$allCategorytypes = self::find()->all();
                    $allCategorytypesArray = ArrayHelper::
                        map($mealRows, 'id', 'categoryTypeName');
                    return $allCategorytypesArray;
                    */
                }
                else
                    return 0;
        } catch (Exception $ex) {
            print_r($e->getMessage());
            return 0;
        }    
    }
}
