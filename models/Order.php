<?php

namespace app\models;

use yii\helpers\ArrayHelper;
use Yii;

/**
 * This is the model class for table "order".
 *
 * @property integer $id
 * @property integer $userId
 * @property integer $mealId
 * @property string $orderDate
 * @property string $lastUpdate
 * @property integer $shift
 * @property integer $categoryTypeId
 * @property integer $categoryId
 * @property integer $status
 * @property string $comment
 *
 * @property User $user
 * @property Categorytype $categoryType
 * @property Category $category
 */
class Order extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'order';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['userId', 'mealId', 'shift', 'categoryTypeId', 'categoryId'], 'required'],
            [['userId', 'mealId', 'shift', 'categoryTypeId', 'categoryId', 'status'], 'integer'],
//            [['orderDate', 'lastUpdate'], 'safe'],
            [['userId', 'mealId', 'orderDate', 'lastUpdate', 'shift', 'categoryTypeId', 'categoryId', 'status', 'comment', 'extras'], 'safe'],
            [['userId', 'mealId', 'shift', 'categoryTypeId', 'categoryId', 'status'], 'integer'],
            [['comment'], 'string', 'max' => 255],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['userId' => 'Id']],
            [['categoryTypeId'], 'exist', 'skipOnError' => true, 'targetClass' => Categorytype::className(), 'targetAttribute' => ['categoryTypeId' => 'id']],
            [['categoryId'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['categoryId' => 'id']],
            [['orderExtras'], 'string', 'max' => 777]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'Id' => 'ID',
            'userId' => 'User ID',
            'mealId' => 'Meal ID',
            'orderDate' => 'Order Date',
            'lastUpdate' => 'Last Update',
            'shift' => 'Shift',
            'categoryTypeId' => 'Category Type ID',
            'categoryId' => 'Category ID',
            'status' => 'Status',
            'comment' => 'Comment',
            'orderExtras' => 'Extras',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    
    public function getVendor() {
        return $this->hasMany(Vendor::className(), ['id' => 'vendorId'])->viaTable('meal', ['id' => 'mealId']);
    }
    
    public function getUser() {
        return $this->hasOne(User::className(), ['Id' => 'userId']);
    }
    
    public function getMeal() {
        return $this->hasOne(Meal::className(), ['id' => 'mealId']);
    }
       

    public function getUserItem() {
        return $this->hasOne(User::className(), ['Id' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryType() {
        return $this->hasOne(Categorytype::className(), ['id' => 'categoryTypeId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory() {
        return $this->hasOne(Category::className(), ['id' => 'categoryId']);
    }
    
    public function getShift() {
        return $this->hasMany(Shift::className(), ['shiftId' => 'shift']);
    }    
/*
    public function getShiftsItem() {
        return $this->hasOne(Shift::className(), ['shiftId' => 'shift']);
    }
*/
    public function getMealItem() {
        return $this->hasOne(Meal::className(), ['id' => 'mealId']);
    }

    public function getStatusItem() {
        return $this->hasOne(Status::className(), ['id' => 'status']);
    }

    //count Order function
    public static function countOrders() {
        return self::find()->count();
    }

    ////////////////////////////

    public function getUserOrders($userID) {
        $orders = array();

        $allOrders = Order::find()
            ->where(['userId' => $userID])
            ->all();

        foreach ($allOrders as $key => $value) {
            switch ($value['shift']) {
                case 1:
                    $value['orderDate'] = date('Y-m-d\TH:i:s\Z', strtotime(substr($value['orderDate'], 0, 10) . ' 08:00:00'));
                    break;
                case 2:
                    $value['orderDate'] = date('Y-m-d\TH:i:s\Z', strtotime(substr($value['orderDate'], 0, 10) . ' 12:00:00'));
                    break;
                case 3:
                    $value['orderDate'] = date('Y-m-d\TH:i:s\Z', strtotime(substr($value['orderDate'], 0, 10) . ' 18:00:00'));
                    break;
                case 4:
                    $value['orderDate'] = date('Y-m-d\TH:i:s\Z', strtotime(substr($value['orderDate'], 0, 10) . ' 23:00:00'));
                    break;
            }

            $mealName = Meal::find()
                ->where(['Id' => $value['mealId']])
                ->one();
            // file_put_contents("/var/www/html/testYair.log", 'key' . $value['id'] . 'title' . $mealName['name'] . 'start' . $value['orderDate'] ."\n" , FILE_APPEND);
            $orders[] = ['key' => $value['id'], 'title' => $mealName['name'], 'start' => $value['orderDate']];
        }

        return $orders;
    }
}
