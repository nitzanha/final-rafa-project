<?php

use yii\db\Migration;

/**
 * Handles the creation of table `status`.
 */
class m171205_164409_create_status_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
      $this->createTable(
            'status',
            [
                'id' => 'pk',
                'name' => 'string'			
            ],
            'ENGINE=InnoDB'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('status');
    }
}
